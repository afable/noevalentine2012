//
//  a_water_monster.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-09.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_WATER_MONSTER_H_
#define A_WATER_MONSTER_H_

#include "a_monster.h"


// init values for water monsters
const float WATER_MONSTER_HEIGHT = 1.0f;	// make monsters smaller than eye so hits work properly
const float WATER_MONSTER_RADIUS = 0.5f;
const float WATER_MONSTER_TRANS_SPEED = 0.05f;
const float WATER_MONSTER_ROT_SPEED = 2.0f;

const int WATER_MONSTER_JUMP_DELAY_MAX = 166;


/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AWaterMonster: a monster that stays in the sea and jumps on approaching boats. gravity and
 *|		total y velocity for a water monster is zero at sea level.
 *| 
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- AMonster: base class for a general monster.
 *|
 *|				- AWaterMonster: a monster that stays in the sea and jumps on approaching boats.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AWaterMonster: public AMonster
{
private:
	
public:
	AWaterMonster(void)
	{
		
	}
	
	~AWaterMonster(void)
	{
		// silent do-nothing destructor
	}
	
	// helper function of AMonster::ProcessLogic() that handles water monster logic
	void ProcessMonsterLogic(void)
	{
		if ( vLocation[1] < ::WATER_LEVEL )	// water monsters float on sea floor
		{
			vLocation[1] = ::WATER_LEVEL;
		}
		
		if ( vLocation[0] < ::SEA_X_MINUS_BOUNDARY ||	// if water monster nears sea rectangle edge, turn monster around
			vLocation[0] > ::SEA_X_PLUS_BOUNDARY ||
			vLocation[2] < ::SEA_Z_MINUS_BOUNDARY ||
			vLocation[2] > ::SEA_Z_PLUS_BOUNDARY )
		{
			MoveForwardX(NEGATIVE(fForwardVelocity));	// first move back
			MoveForwardZ(NEGATIVE(fForwardVelocity));
			boundingCircle.Update(vLocation);
			
			fRotY += 180.0f;	// then turn around
			
			fRotY += 65.0f - (rand() * 0.0000000005f * 120.0f);	// generate a random turn value between ~[-55, 55] degrees
		}
		
		
		if ( bEdge || bCorner || bBotCollision )	// if at edge or water monster on land, jump!
		{
			if ( bCanJump ) // only jump if monster can
			{
				bCanJump = false;	// monster can't jump while in the air
				fUpVelocity = JUMP_SPEED;	// increase y velocity
				nJumpDelay = WATER_MONSTER_JUMP_DELAY_MAX;	// reset your next jump delay to max
			}
			bEdge = false;
			bCorner = false;
		}
	}
	
	
	
	
	
};	// end of class AWaterMonster

#endif	// A_WATER_MONSTER_H_


