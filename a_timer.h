//
//  a_timer.h
//
//  Created by afable on 12-02-13.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_TIMER_H_
#define A_TIMER_H_

#include <iostream>
#include <SDL/SDL.h>

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	ATimer: a simple timer to control the main loops framerate
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class ATimer
{
private:
	unsigned int start_ticks_;
	unsigned int paused_ticks_;
	bool started_;
	bool paused_;
public:
	ATimer(void);	// overwrite default constructor
	~ATimer(void);	// overwrite default destructor
	void Start(void);
	void Stop(void);
	void Pause(void);
	void UnPause(void);
	int GetTicks(void) const;
	bool IsStarted(void);
	bool IsPaused(void);
	
	friend std::ostream& operator << (std::ostream& os, const ATimer& t);
	
}; // end of class ATimer



#endif // A_TIMER_H_


