//
//  a_eye.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-02.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


#ifndef A_EYE_H_
#define A_EYE_H_

#include "a_actor.h"

// eye init values
const int EYE_ID = 9999;
const float EYE_X_LOC = 0.0f;
const float EYE_Y_LOC = 2.0f;
const float EYE_Z_LOC = 0.0f;
const float EYE_HEIGHT = 1.0f;
const float EYE_RADIUS = 0.25f;
const float EYE_TRANS_SPEED = 0.02f;
const float EYE_ROT_SPEED = 0.2f;
const float EYE_X_ROT = 0.0f;
const float EYE_Y_ROT = 0.0f;

// when monster hits eye
const int HIT_DELAY_MAX = 26;
const float HIT_SPEED = 0.1f;



/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AEye: actor holds the position and direction of OpenGL's fake eye. eye's pov (look into world)
 *|		differs from other actor's pov (being looked at from an eye) thus, all interaction with the
 *|		eye needs to be done in the opposite direction (i.e. move world backwards same as move eye
 *|		forwards)
 *|
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- AEye: actor holds the position and direction of OpenGL's fake eye.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AEye: public AActor
{
private:
	
public:
	/* STATIC VARIABLES */
	static bool bResetEye;	// reset boat and elevator with eye
	static bool bNorthIslandCheckpoint;	// if eye reaches the north island, allow update to reset values
	static bool bCheckpointNotReached;	// to mark and print that the checkpoint has been reached
	
	// additional hit data
	AVector vHit;	// vector after being hit
	float fHitVelocity;		// velocity after being hit
	int nHitDelay;		// delay of inactivity after being hit
	
	/* CONSTRUCTORS & DESTRUCTOR */
	AEye(void) : AActor()
	{
		// Init() should be used to intialize eye values
	}
	
	~AEye(void)
	{
		// silent do-nothing destructors
	}
	
	/* VIRTUAL FUNCTION REDEFINITIONS */
	void Init(int eyeId, float xloc, float yloc, float zloc, float height, float radius, float transSpeed, float rotSpeed, float xrot, float yrot)
	{
		nId = eyeId;	// means not initialized
		
		// eye position, size, and orientation
		vLocation.Set(xloc, yloc, zloc, 1.0f);
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = xrot;
		fRotY = yrot;
		
		// collision data
		boundingCircle.Init(vLocation, radius);		// eye only uses a bounding circle
		
		// velocities and speeds
		fGravity = GRAVITY;
		fTransSpeed = transSpeed;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		fRotSpeed = rotSpeed;
		fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
		
		// set reset values
		fXLoc = xloc;
		fYLoc = yloc;
		fZLoc = zloc;
		fXRot = xrot;
		fYRot = yrot;
		
		// set checkpoints
		bNorthIslandCheckpoint = false;
		bCheckpointNotReached = true;
	}
	
	void Reset(void)
	{
		// reset eye position based on checkpoints reached
		if ( bNorthIslandCheckpoint )
		{
			vLocation.Set(0.0f, 3.0f, -63.5f);
			fRotX = 0.0f;
			fRotY = 0.0f;
		}
		else
		{
			vLocation.Set(fXLoc, fYLoc, fZLoc, 1.0f);
			fRotX = fXRot;
			fRotY = fYRot;
		}
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		
		// collision data
		boundingCircle.Init(vLocation, ::EYE_RADIUS);
		
		// velocities and speeds
		fGravity = GRAVITY;
		//fTransSpeed = transSpeed;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		//fRotSpeed = rotSpeed;
		fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
		
		// set reset to true so that it will be flagged in main
		bResetEye = true;
	}
	
	// process incoming events: handle events (key input) and set object velocities
	void ProcessEvents(SDL_Event& event)
	{
		// after grabbing an event from the event queue
		switch (event.type)
		{		
			case SDL_KEYDOWN: switch (event.key.keysym.sym)
			{
				case SDLK_w:
					if ( !bHit ) fForwardVelocity = fTransSpeed; // movement in world coordinate system if not hit
					else fForwardVelocity = 0.0f;
					break;
				case SDLK_s:
					if ( !bHit ) fBackwardVelocity = NEGATIVE(fTransSpeed); // movement back if not hit
					else fBackwardVelocity = 0.0f;
					break;
				case SDLK_a:
					if ( !bHit ) fLeftVelocity = NEGATIVE(fTransSpeed); // movement left if not hit
					else fLeftVelocity = 0.0f;
					break;
				case SDLK_d:
					if ( !bHit )fRightVelocity = fTransSpeed; // movement right if not hit
					else fRightVelocity = 0.0f;
					break;
				case SDLK_SPACE:
					SET_MULTI_JUMP
					(
					 if ( bCanJump && !bHit ) // only jump if you can! and you haven't been hit
					 {
						 bCanJump = false;	// you can't jump while you're in the air
						 fUpVelocity = JUMP_SPEED;	// increase y velocity
						 nJumpDelay = JUMP_DELAY_MAX;	// reset your next jump delay to max
					 }
					 )
										
					if ( bCanJump && !bHit && bBotCollision ) // only jump if you can! and you haven't been hit
					{
						bCanJump = false;	// you can't jump while you're in the air
						fUpVelocity = JUMP_SPEED;	// increase y velocity
						nJumpDelay = JUMP_DELAY_MAX;	// reset your next jump delay to max
					}
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_KEYUP: switch (event.key.keysym.sym)
			{
				case SDLK_w: fForwardVelocity = 0.0f;
					break;
				case SDLK_s: fBackwardVelocity = 0.0f;
					break;
				case SDLK_a: fLeftVelocity = 0.0f;
					break;
				case SDLK_d: fRightVelocity = 0.0f;
					break;
				case SDLK_SPACE: // jump is a press once deal
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_MOUSEMOTION:
				if ( event.motion.state == 1 || event.motion.state == 4 )	// if left or right mouse button clicked
				{
					// add relative x, y positions to eye-y, eye-x rotations (actually subtract because shifting the world down is like moving the eye up)
					fRotY MINUS_EQUALS (GLfloat)event.motion.xrel * fRotSpeed; // rotate world left = rotate eye right
					fRotX MINUS_EQUALS (GLfloat)event.motion.yrel * fRotSpeed; // rotate world down = rotate eye up
				}
				break;
				
			case SDL_MOUSEBUTTONDOWN: // do something if mouse is clicked?
				break;
				
			default:
				break;
		}
	}


	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| EYE TO BLOCK/ELEVATOR COLLISIONS IN Y, X, Z
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	// helper function to ProcessLogic() that processes any block type collisions in y
	void ProcessEye2BlockCollisionY(const AActor* block)
	{
		/**************************************************
		 * EYE TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS Y: collisions with blocks outter bounding circle
		 **************************************************/	
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * EYE TO BLOCK CIRCLE-TO-SQUARE COLLISIONS Y: collisions with blocks inner bounding box
			 **************************************************/
			if ( Circle2SquareCollision(boundingCircle, block->boundingBox) )	// if not while since y logic only happens once a frame and only if there is a collision
			{
				bCollision = true;
				
				/**************************************************
				 * EYE TO BLOCK COLLISION Y: 4 cases: eye high above or far below block, eye standing
				 *	ontop of block, eye headbutting block from below, or eye pushed into block by
				 *	monster.
				 **************************************************/
				if ( fBotY > block->fTopY + absv(fDownVelocity + fGravity) ||
					fTopY < block->fBotY - JUMP_SPEED )	// eye high above block or far below block
				{
					// allow the y movement since eye very high or far low from this block (already updated)
				}
				else if ( fBotY >= block->fTopY - absv(fGravity + fDownVelocity) )	// else if eye standing ontop of block
				{
					// might have landed a block from a jump, reset up velocity
					fUpVelocity = 0.0f;
					
					if ( bBotCollision )	// if there was already a collision with blocks (e.g. eye standing between two blocks in same y value) don't translate back since it has already been done
					{
						// do nothing since has been handled already
					}
					else	// if this is the first block to collide with eye's bottom, translate eye back in y (stand on block or fall from hitting block)
					{
						while ( fBotY <= block->fTopY )	// keep eye ontop of block
						{
							MoveUpY(absv(fDownVelocity) + ::COLLISION_SPEED);
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
							--nSquished;
							
							if ( nSquished < 0 ) // if eye has been squished between two blocks;
							{
								DEBUGGING
								(
								 std::cerr << *this << std::endl;
								 std::cerr << *block << std::endl;
								 for (int i=0;i<999;++i) std::cerr << "resetting from eye standing on block y\n";
								 )
								Reset();
								break;	// while ( fTopY >= block->fBotY )
							}
						}
						bBotCollision = true;		// handle eye on multiple blocks
						
						
					}
				}
				else if ( fTopY <= block->fBotY + JUMP_SPEED )	// eye's head butting block, use gravity and jump speed for relative error (jump speed gives us max range eye will headbutt block)
				{
					if ( bTopCollision )	// handle eye headbutting multiple blocks
					{
						// do nothing since handled already
					}
					else	// first time eye is headbutting a block in this frame
					{
						while ( fTopY >= block->fBotY )	// handle being squished up
						{
							fUpVelocity = -fUpVelocity;	// change up velocity direction to down!
							MoveUpY(fUpVelocity + fDownVelocity + GRAVITY);	// want eye to move down from headbutt
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
							bTopCollision = true;
							
							--nSquished;
							if ( nSquished < 0 ) // if eye has been squished between two blocks;
							{
								DEBUGGING
								(
								 std::cerr << *this << std::endl;
								 std::cerr << *block << std::endl;
								 for (int i=0;i<999;++i) std::cerr << "resetting from eye headbutting block y\n";
								 )
								Reset();
								break;	// while ( fTopY >= block->fBotY )
							}
						}
					}
				}
				else	// eye can only get here if hit into a block by monster
				{
					// get vector from block to eye and move eye away in y from block
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					if ( fBotY <= block->fTopY + ERROR_RANGE_SMALL &&
						fBotY >= block->fTopY - ERROR_RANGE_SMALL )	// case eye is above block
					{
						while ( fBotY <= block->fTopY + ERROR_RANGE_SMALL )
						{
							MoveDirectionY(pusher2pushee, ::COLLISION_SPEED);
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
						}
					}
					else if ( fTopY >= block->fBotY - ERROR_RANGE_SMALL &&
							 fTopY <= block->fBotY + ERROR_RANGE_SMALL )	// case eye is below block
					{
						while ( fTopY >= block->fBotY - ERROR_RANGE_SMALL )
						{
							MoveDirectionY(pusher2pushee, ::COLLISION_SPEED);
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
						}
					}
				}
			}
		}
	}
	
	// helper function to ProcessLogic() that processes any block type collisions in X
	void ProcessEye2BlockCollisionX(const AActor* block)
	{
		/**************************************************
		 * EYE TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS X: collisions with blocks outter bounding circle
		 **************************************************/
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{	
			/**************************************************
			 * EYE TO BLOCK CIRCLE-TO-SQUARE COLLISIONS X: collisions with blocks inner bounding box
			 **************************************************/
			while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
			{
				bCollision = true;
				
				/**************************************************
				 * EYE TO BLOCK COLLISION X: divide into 2 cases, either eye is on
				 *	a level that can interact with block (bot of eye below top of block or top
				 *	of eye above bot of block) or eye is above/below that level
				 **************************************************/
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// level eye can interact with block
				{
					bInteractCollision = true;
					
					// get vector from block to eye
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					// find the larger component and translate back in that direction (directly away from block)
					if ( absv(pusher2pushee[0]) > absv(pusher2pushee[2]) )
						MoveDirectionX(pusher2pushee, COLLISION_SPEED + fHitVelocity); // add hit velocity in case hit
					else
						MoveDirectionZ(pusher2pushee, COLLISION_SPEED + fHitVelocity); // add hit velocity in case hit

					boundingCircle.Update(vLocation);
					
					if ( bHit )		// if coming off a hit from a monster, keep eye collided with wall
					{
						vHit.Set(0.0f, 0.0f, 0.0f);
						MoveDirectionY(pusher2pushee, fHitVelocity);	// ensure eye isn't hit into ground
						fTopY = vLocation[1] + fHalfHeight;
						fBotY = vLocation[1] - fHalfHeight;
					}
				}
				else	// level eye cannot interact with block
				{
					// allow x movements
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}

				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from eye to block x\n";
					 )
					Reset();
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}
			}
		}
	}
	
	// helper function to ProcessLogic() that processes any block type collisions in Z
	void ProcessEye2BlockCollisionZ(const AActor* block)
	{
		/**************************************************
		 * EYE TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS Z: collisions with blocks outter bounding circle
		 **************************************************/
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * EYE TO BLOCK CIRCLE-TO-SQUARE COLLISIONS Z: collisions with blocks inner bounding box
			 **************************************************/
			while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
			{
				bCollision = true;
				
				/**************************************************
				 * EYE TO BLOCK COLLISION Z: divide into 2 cases, either eye is on
				 *	a level that can interact with block (bot of eye below top of block or top
				 *	of eye above bot of block) or eye is above/below that level
				 **************************************************/
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// level eye can interact with block
				{
					bInteractCollision = true;
					
					// get vector from block to eye
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					// find the larger component and translate back in that direction (directly away from block)
					if ( absv(pusher2pushee[0]) > absv(pusher2pushee[2]) )
						MoveDirectionX(pusher2pushee, ::COLLISION_SPEED + fHitVelocity); // add hit velocity in case hit
					else
						MoveDirectionZ(pusher2pushee, ::COLLISION_SPEED + fHitVelocity); // // add hit velocity in case hit
					boundingCircle.Update(vLocation);
					
					if ( bHit )		// if coming off a hit from a monster, keep eye collided with wall
					{
						vHit.Set(0.0f, 0.0f, 0.0f);
						MoveDirectionY(pusher2pushee, fHitVelocity);	// ensure eye isn't hit into ground
						fTopY = vLocation[1] + fHalfHeight;
						fBotY = vLocation[1] - fHalfHeight;
					}
				}
				else	// level eye cannot interact with block
				{
					// allow z movements
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from eye to block z\n";
					 )
					Reset();
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}
			}
		}
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| EYE 2 MONSTER COLLISIONS IN X, Y, Z
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessEye2MonsterCollisionY(AActor* monster)
	{
		/**************************************************
		 * EYE TO MONSTERS CIRCLE-TO-CIRCLE COLLISION Y: collisions with monsters outter bounding circle
		 **************************************************/
		// test collision with monster's outter bounding circle
		if ( Circle2CircleCollision(boundingCircle, monster->boundingCircle) )
		{
			bCollision = true;
			
			/**************************************************
			 * EYE TO HEART COLLISION Y: if monster has been turned into a heart, try to take it!
			 **************************************************/
			if ( monster->bHeart )
			{
				if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL + absv(fDownVelocity) ||
					fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above or very low below heart
				{
					// allow y translation since eye cannot interact with heart
				}
				else	// eye can take the heart!
				{
					monster->bDelete = true;
				}
				return;
			}
			
			/**************************************************
			 * EYE TO MONSTER COLLISION Y: 4 cases: eye is high above or below monster and cannot
			 *	interact, eye is right ontop of monster and can kill monster, eye is headbutting monster
			 *	from below, or eye is at a level that can interact with monster (get hit by monster).
			 **************************************************/
			
			if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL + absv(fDownVelocity) ||
				fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above monster or very low below monster
			{
				// allow y translation since eye cannot interact with monster
			}
			else if ( fBotY >= monster->fTopY + fDownVelocity )	// else if eye is actually standing ontop of monster
			{
				if ( bBotCollision )	// if eye has already stomped on a monster, don't increase jump twice for multiple monsters
				{
					// do nothing since a jump from a monster has already been handled
				}
				else	// kill monster and turn it into a heart
				{
					bBotCollision = true;
					
					// first move eye above monster
					while ( fBotY <= monster->fTopY + ::ERROR_RANGE_NORMAL )
					{
						MoveUpY(::COLLISION_SPEED);	// move eye above monster
						fTopY = vLocation[1] + fHalfHeight;
						fBotY = vLocation[1] - fHalfHeight;
					}
					
					if ( bCanJump && !bHit ) // only jump if you can! and you haven't been hit
					{
						bCanJump = false;	// you can't jump while you're in the air
						fUpVelocity = JUMP_SPEED;	// increase y velocity
						nJumpDelay = JUMP_DELAY_MAX;	// reset your next jump delay to max
					}
					
					monster->bHeart = true;	// turn monster into a heart!
				}
			}
			else if ( fTopY <= monster->fBotY + ::ERROR_RANGE_NORMAL )	// eye headbutting monster from below
			{
				bTopCollision = true;
				
				// set eye to hit
				bHit = true;
				nHitDelay = HIT_DELAY_MAX;
				fHitVelocity = HIT_SPEED;
				
				// set the vector that will direct the hit eye for nHitDelay frames
				Subtract(vHit, vLocation, monster->vLocation);
				
				while ( fTopY >= monster->fBotY - ::ERROR_RANGE_SMALL ) // move eye down until far below monster
				{
					MoveUpY(NEGATIVE(fHitVelocity));
					fTopY = vLocation[1] + fHalfHeight;
					fBotY = vLocation[1] - fHalfHeight;
				}
			}
			else	// eye at level that can interact (be hit by) monster, monster has moved into eye
			{
				// set eye to hit
				bHit = true;
				nHitDelay = HIT_DELAY_MAX;
				fHitVelocity = HIT_SPEED;
				
				// set the vector that will direct the hit eye for nHitDelay frames
				Subtract(vHit, vLocation, monster->vLocation);
				
				// translate eye away from monster in x,z
				MoveDirectionX(vHit, fHitVelocity);
				MoveDirectionZ(vHit, fHitVelocity);
				boundingCircle.Update(vLocation);
			}
		}
	}
	
	void ProcessEye2MonsterCollisionX(AActor* monster)
	{
		/**************************************************
		 * EYE TO MONSTERS CIRCLE-TO-CIRCLE COLLISION X: collisions with monsters outter bounding circle
		 **************************************************/
		// test collision with monster's outter bounding circle
		if ( Circle2CircleCollision(boundingCircle, monster->boundingCircle) )
		{
			bCollision = true;
			
			/**************************************************
			 * EYE TO HEART COLLISION X: if monster has been turned into a heart, try to take it!
			 **************************************************/
			if ( monster->bHeart )
			{
				if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL ||
					fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above or very low below heart
				{
					// allow y translation since eye cannot interact with heart
				}
				else	// eye can take the heart!
				{
					monster->bDelete = true;
				}
				return;
			}
			
			/**************************************************
			 * EYE TO MONSTER COLLISION X: 2 cases: eye is at a level that can interact with monster
			 *	(get hit by monster), eye is high above or below monster and cannot interact
			 **************************************************/
			
			if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL ||
				fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above monster or very low below monster
			{
				// allow x translation since eye cannot interact with monster
			}
			else		// eye hit by monster
			{
				// set eye to hit
				bHit = true;
				nHitDelay = HIT_DELAY_MAX;
				fHitVelocity = HIT_SPEED;

				// set the vector that will direct the hit eye for nHitDelay frames
				Subtract(vHit, vLocation, monster->vLocation);
				
				while ( Circle2CircleCollision(boundingCircle, monster->boundingCircle) ) // eye hit from monster
				{
					MoveDirectionX(vHit, fHitVelocity);
					MoveDirectionZ(vHit, fHitVelocity);
					boundingCircle.Update(vLocation);
				}
			}
		}
	}
	
	void ProcessEye2MonsterCollisionZ(AActor* monster)
	{
		/**************************************************
		 * EYE TO MONSTERS CIRCLE-TO-CIRCLE COLLISION Z: collisions with monsters outter bounding circle
		 **************************************************/
		// test collision with monster's outter bounding circle
		if ( Circle2CircleCollision(boundingCircle, monster->boundingCircle) )
		{
			bCollision = true;
			
			/**************************************************
			 * EYE TO HEART COLLISION Z: if monster has been turned into a heart, try to take it!
			 **************************************************/
			if ( monster->bHeart )
			{
				if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL ||
					fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above or very low below heart
				{
					// allow y translation since eye cannot interact with heart
				}
				else	// eye can take the heart!
				{
					monster->bDelete = true;
				}
				return;
			}
			
			/**************************************************
			 * EYE TO MONSTER COLLISION Z: 2 cases: eye is high above or below monster and cannot
			 *	interact with monster or eye is at a level that can interact with monster
			 *	(get hit by monster)
			 **************************************************/
			
			if ( fBotY > monster->fTopY + ERROR_RANGE_SMALL ||
				fTopY < monster->fBotY - ERROR_RANGE_SMALL )	// eye very high above monster or very low below monster
			{
				// allow x translation since eye cannot interact with monster
			}
			else		// eye hit by monster
			{
				// set eye to hit
				bHit = true;
				nHitDelay = HIT_DELAY_MAX;
				fHitVelocity = HIT_SPEED;
				
				// set the vector that will direct the hit eye for nHitDelay frames
				Subtract(vHit, vLocation, monster->vLocation);
				
				while ( Circle2CircleCollision(boundingCircle, monster->boundingCircle) ) // eye hit by monster
				{
					MoveDirectionX(vHit, fHitVelocity);
					MoveDirectionZ(vHit, fHitVelocity);
					boundingCircle.Update(vLocation);
				}
			}
		}
	}

	// helper function of AEye::ProcessLogic() that updates eye's reset values as eye reaches checkpoints
	void ProcessCheckPoints(void)
	{
		if ( vLocation[2] < -63.0f && bCheckpointNotReached )	// checkpoint 1 reached
		{
			DEBUGGING( std::cerr << "Checkpoint Reached. You will now respawn at the northern island.\n"; )
			bNorthIslandCheckpoint = true;
			bCheckpointNotReached = false;
		}
	}
	
	// helper function of AEye::ProcessLogic() that handles -y boundary (eye drowned state)
	void ProcessEyeLogic(void)
	{
		if ( vLocation[1] < ::SEA_Y_BOUNDARY )
		{
			DEBUGGING ( std::cerr << "eye just drowned\n\n"; )
			Reset();
		}
	}
	
	// helper function of AEye::ProcessLogic() that makes eye fall faster as time passes
	void ProcessGravity(void)
	{
		if ( bBotCollision )	// if eye is falling (not standing on something), make gravity pull faster down!
			fDownVelocity = 0.0f;
		else
			fDownVelocity += ::DOWN_ACCELERATION;
	}
	
	
	// helper function of AEye::ProcessLogic() that handles jumps
	void ProcessJumps(void)
	{	
		// decrement the jump delay
		--nJumpDelay;
		
		// if the jump delay has reached zero, eye is allowed to jump again
		if ( nJumpDelay < 0 )
			bCanJump = true;
		
		// decrement jump velocity
		fUpVelocity += JUMP_DECREMENT;
		
		// keep up velocity non-negative
		if ( fUpVelocity < 0.0f )
			fUpVelocity = 0.0f;
	}
	
	// helper function of AEye::ProcessLogic() that handles eye hit by monsters
	void ProcessHits(void)
	{
		if ( bHit ) // set jump velocities to 0 if hit
		{
			fUpVelocity = 0.0f;
			fDownVelocity = 0.0f;
		}
		
		// decrement the hit delay
		--nHitDelay;
		
		// if the hit delay has reached zero, eye is allowed to move again and reset hit velocity
		if ( nHitDelay < 0 )
		{
			bHit = false;
			fHitVelocity = 0.0f;
		}
		
		// translate eye by current hit vector and udpate
		MoveDirectionY(vHit, fHitVelocity);
		MoveDirectionX(vHit, fHitVelocity);
		MoveDirectionZ(vHit, fHitVelocity);
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		boundingCircle.Update(vLocation);
	}
	
	void BindEyeRotX(void)
	{
		if ( fRotX > 90.0f && fRotX < 180.0f )	// bind eye rotation 90 degrees up
			fRotX = 90.0f;
		if ( fRotX >= 180.0f && fRotX < 270.0f )
			fRotX = 270.0f;		// bind eye rotation -90 degrees down
	}

	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| PROCESS LOGIC: 	move objects, check collisions (and move back if necessary)
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessLogic(const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators, const std::vector<AActor*>& monsters, const std::vector<AActor*>& waterMonsters)
	{	
		// update checkpoints that eye has reached
		ProcessCheckPoints();
		
		// process eye logic
		ProcessEyeLogic();
		
		// process gravity
		ProcessGravity();
		
		SET_GRAVITY_CONSTANT
		(
		 fDownVelocity = 0.0f;
		 )
		
		// process jumps that the eye can make
		ProcessJumps();
		
		// process hits
		ProcessHits();
		
		// add rotational velocities
		//fRotY += fRotYVelocity;	// eye rotational velocities are added in ProcessEvents()
		
		// reset unit vectors (leave location alone) and bound angles for sin/cos lookup tables
		LoadIdentity();
		
		// bound the eye's xrot to max 90 degrees like a normal human
		BindEyeRotX();
		
		// rotate forward and right vectors
		RotateY(fRotY);
		
		// reset squish amount to maximum
		nSquished = SQUISH_AMOUNT_MAX;
		
		// reset all collisions as false
		bCollision = false;
		bInteractCollision = false;	// handles eye's upper body colliding with blocks
		bTopCollision = false;	// handles eye ontop of many blocks
		bBotCollision = false;	// handles eye headbutting into many blocks
		bEdge = false;	// tests whether eye is at a block's edge
		bCorner = false;	// tests if eye is at a block's corner (rare to hit only a corner and no edge but possible)

		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN Y: move eye in y, update top and bot eye boundaries, and handle jumps. a jump
		 *|		delay is used to ensure that a jump happens at the press of the spacebar and jumps
		 *|		can only be pressed if you are ontop or climbing an actor. next, test collisions with
		 *|		other actors and move back in y if you hit another actor
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate eye up and down by up, down velocities
		MoveUpY(fUpVelocity + fDownVelocity + fGravity); //  vForward={x,0,z}, vRight={x,0,z}, no y component
		
		// update top and bottom boundaries based on actor's height (only y translations affect this)
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		
		/**************************************************
		 * EYE TO BLOCK COLLISIONS Y: process eye to block y collisions first. the y
		 *	collisions are split into 5 categories: eye high above block, eye ontop of block, eye
		 *	on block level, eye headbutting block, eye far below block.
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i )	// process every block
			ProcessEye2BlockCollisionY(blocks[i]);
		
		/**************************************************
		 * EYE TO ELEVATOR COLLISIONS Y: process eye to elevator y collisions. the y
		 *	collisions are split into 5 categories: eye high above block, eye ontop of block, eye
		 *	on block level, eye headbutting block, eye far below block.
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessEye2BlockCollisionY(elevators[i]);
		
		/**************************************************
		 * EYE TO MONSTER COLLISIONS Y: monster collisions split into 3 categories: eye high above
		 *	monster, eye ontop of monster, eye on monster level.
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i )
			ProcessEye2MonsterCollisionY(monsters[i]);
		
		/**************************************************
		 * EYE TO WATER MONSTER COLLISIONS Y: same as eye2monster collisions above
		 **************************************************/
		for ( unsigned int i = 0; i < waterMonsters.size(); ++i )
			ProcessEye2MonsterCollisionY(waterMonsters[i]);
		

		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN X: move eye in x, update collision circle, test x collisions with actors
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate forward by forward velocity amount
		MoveForwardX(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightX(fRightVelocity + fLeftVelocity);
		
		// update bounding circles (eye only uses a bounding circle and only xz translations affect this)
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * EYE TO BLOCK COLLISIONS X: test collisions between eye and blocks outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	eye is above, on-top of, below, or on same level as block
		 **************************************************/
		// process eye to block collisions
		for ( unsigned int i = 0; i < blocks.size(); ++i )
			ProcessEye2BlockCollisionX(blocks[i]);
		
		/**************************************************
		 * EYE TO ELEVATOR COLLISIONS X: test collisions between eye and elevators outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	eye is above, on-top of, below, or on same level as elevators
		 **************************************************/
		// process eye to elevator collisions
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessEye2BlockCollisionX(elevators[i]);
		
		/**************************************************
		 * EYE TO MONSTER COLLISIONS X: test collisions between eye and monsters bounding circle.
		 *	then check y value to determine if eye is above, on-top of, below, or on same level
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i )
			ProcessEye2MonsterCollisionX(monsters[i]);
		
		/**************************************************
		 * EYE TO WATER MONSTER COLLISIONS X: same as eye2monster collisions above
		 **************************************************/
		for ( unsigned int i = 0; i < waterMonsters.size(); ++i )
			ProcessEye2MonsterCollisionX(waterMonsters[i]);
		
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN Z: move eye in z, update collision circle, test z collisions with actors
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate forward by forward velocity amount
		MoveForwardZ(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightZ(fRightVelocity + fLeftVelocity);

		// update bounding circles (eye only uses a bounding circle and only xz translations affect this)
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * EYE TO BLOCK COLLISIONS Z: test collisions between eye and blocks outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	eye is above, on-top of, below, or on same level as block
		 **************************************************/
		// process eye to block collisions
		for ( unsigned int i = 0; i < blocks.size(); ++i )
			ProcessEye2BlockCollisionZ(blocks[i]);
		
		/**************************************************
		 * EYE TO ELEVATOR COLLISIONS Z: test collisions between eye and elevators outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	eye is above, on-top of, below, or on same level as elevators
		 **************************************************/
		// process eye to elevator collisions
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessEye2BlockCollisionZ(elevators[i]);
		
		/**************************************************
		 * EYE TO MONSTER COLLISIONS Z: test collisions between eye and monsters bounding circle.
		 *	then check y value to determine if eye is above, on-top of, below, or on same level
		 **************************************************/
		for ( unsigned int i = 0; i < monsters.size(); ++i )
			ProcessEye2MonsterCollisionZ(monsters[i]);
		
		/**************************************************
		 * EYE TO WATER MONSTER COLLISIONS Z: same as eye2monster collisions above
		 **************************************************/
		for ( unsigned int i = 0; i < waterMonsters.size(); ++i )
			ProcessEye2MonsterCollisionZ(waterMonsters[i]);
	}
	
	// rotate and translate world (for eye) and render images to screen
	void Display(GLuint lDrawing)
	{
		// rotate drawer before drawing object to appear as if camera is rotating around object (negative from eye's perspective)
		glRotatef(NEGATIVE(fRotX), 1.0f, 0.0f, 0.0f);
		
		// draw 3ppov object before y rotation so position object constantly if eye turns left & right
		glCallList(lDrawing);
		
		// rotate drawer in y after drawing object so object is always facing forward (negative from eye's perspective)
		glRotatef(NEGATIVE(fRotY), 0.0f, 1.0f, 0.0f);
		
		// translate drawer away from eye to draw other objects (everything negative from eye's perspective)
		glTranslatef(NEGATIVE(vLocation[0]), NEGATIVE(vLocation[1]), NEGATIVE(vLocation[2])); // to move forward, move drawer backwards (everything drawn away from eye so negative z)
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const AEye& e)
	{
		os << "** eye **: " << e.nId << std::endl;
		os << "\tvLocation: " << e.vLocation << std::endl;
		os << "Velocities (x, y, z): (" << e.fRightVelocity + e.fLeftVelocity << "," << e.fUpVelocity + e.fDownVelocity << "," << e.fForwardVelocity + e.fBackwardVelocity << "), fGravity = " << e.fGravity << "\n";
		os << "fGravity: " << e.fGravity << std::endl;
		os << "boundingCircle:\n";
		os << e.boundingCircle << std::endl;
		os << "bCollision: " << e.bCollision << std::endl;		
		os << "bTopCollision: " << e.bTopCollision << std::endl;
		os << "bBotCollision: " << e.bBotCollision << std::endl;
		os << "bInteractCollision: " << e.bInteractCollision << std::endl;
		os << "bEdge: " << e.bEdge << std::endl;
		os << "bCorner: " << e.bCorner << std::endl;
		os << "nSquished: " << e.nSquished << std::endl;		
		os << "bCanJump: " << e.bCanJump << std::endl;
		os << "nJumpDelay: " << e.nJumpDelay << std::endl;
		os << "bHit: " << e.bHit << std::endl;
		os << "nHitDelay: " << e.nHitDelay << std::endl;
		os << "fTopY: " << e.fTopY << ", fBotY: " << e.fBotY << std::endl;
		
		return os;
	}
	
	
	
	
	
	
	
}; // end of class AEye








#endif	// A_EYE_H_



