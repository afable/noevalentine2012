//
//  a_preprocessor_defines.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


#define NEGATIVE -
#define MINUS_EQUALS -=


/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	MACRO DEFINITIONS
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
#define DEBUGGING_ON_
//#define CONSTANT_GRAVITY_ON_
#define FLYING_BOAT_ON_
//#define MULTI_JUMP_ON_


/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	MACRO FUNCTIONS
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
// turn on debugging output to std error (terminal)
#ifdef DEBUGGING_ON_
	#define DEBUGGING(x) { x }
#else
	#define DEBUGGING(x)
#endif

// turn off downwards acceleration and use a constant gravity
#ifdef CONSTANT_GRAVITY_ON_
	#define SET_GRAVITY_CONSTANT(x) { x }
#else
	#define SET_GRAVITY_CONSTANT(x)
#endif

// allow boat to move in y
#ifdef FLYING_BOAT_ON_
	#define FLYING(x) { x }
#else
	#define FLYING(x)
#endif

// allow jump only when eye is standing on block
#ifdef MULTI_JUMP_ON_
	#define SET_MULTI_JUMP(x) { x }
#else
	#define SET_MULTI_JUMP(x)
#endif







