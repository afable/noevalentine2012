//
//  a_actor.h
//  prog0-2_valentines
//
//  Created by afable on 12-04-28.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_ACTOR_H_
#define A_ACTOR_H_

#include <iostream>
#include <SDL/SDL.h>	// for handling events
#ifdef __linux__
	#include <GL/gl.h>
	#include <GL/glu.h>
#else
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#endif
#include <vector>		// for collision checking (refer to SDL prog19-1)
#include <stdio.h>		// srand
#include <time.h>		// seed srand with time(NULL)
#include "a_matrix.h"
#include "a_preprocessor_defines.h"
#include "a_vector.h"
#include "xz_circle.h" // for circle collision detection
#include "xz_square.h" // for square collision detection


// world boundaries
const float WATER_LEVEL = -0.1f;
const float SEA_X_PLUS_BOUNDARY = 10.0f;
const float SEA_X_MINUS_BOUNDARY = -10.0f;
const float SEA_Z_PLUS_BOUNDARY = 10.0f;
const float SEA_Z_MINUS_BOUNDARY = -70.0f;
const float SEA_Y_BOUNDARY = -3.0f;
const float SKY_LEVEL = 40.5f;

// gravity and accelerations
const float GRAVITY = -0.02f;
const float ZERO_GRAVITY = 0.0f;
const float DOWN_ACCELERATION = -0.0005f;

// error ranges are used in calculating y value ranges in ProcessLogic() collisions
const float ERROR_RANGE_NORMAL = 0.04f;
const float ERROR_RANGE_SMALL = 0.02f;
const float ERROR_RANGE_SMALLEST = 0.01f;

// speed to move back from actor2block collisions
const float COLLISION_SPEED = 0.005f;

// determine how many frames until actor must be reset from being squished between two walls
const int SQUISH_AMOUNT_MAX = 33;

// actor jump values
const float JUMP_DECREMENT = -0.001f;
const int JUMP_DELAY_MAX = 33;
const float JUMP_SPEED = 0.08f;

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AActor: base class for all actors within the scene including 'fake' eye.
 *| 
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- ABlock: a static block that makes up the ground terrain and island terrain.
 *|
 *|				- AElevator: moving blocks that make up moving platforms within game.
 *|
 *|					- ABoat: a special kind of elevator: moving blocks that are player controlled transport vessels.
 *|
 *|			- AEye: actor holds the position and direction of OpenGL's fake eye.
 *|
 *|			- AMonster: base class for a general monster.
 *|
 *|				- AWaterMonster: a monster that stays in the sea and jumps on approaching boats.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
// A GL orthonormal frame class
class AActor
{
private:
	
public:
	/*************************************************
	 * STATIC VARIABLES: statics generally used to avoid construction calls when performing
	 *	actor movements and collision detections
	 *************************************************/
	/* column ordered matrix:
	 *		right[0],	up[0],	fwd[0],	translation[0],
	 *		right[1],	up[1],	fwd[1],	translation[1],
	 *		right[2],	up[2],	fwd[2],	translation[2],
	 *		0.0f,		0.0f,	0.0f,	1.0f;
	 */
	static AMatrix m;
	static AVector vTemp;
	
	// box1 temp points in world coords
	static APoint left;		// leftmost vertex for box1
	static APoint right;	// rightmost vertex, etc...
	static APoint up;
	static APoint down;
	
	// collision test vectors for box1
	static AVector l2d;
	static AVector d2r;
	static AVector r2u;
	static AVector u2l;

	// box2 temp points in world coords
	static APoint left2;	// leftmost vertex for for box2, etc.
	static APoint right2;
	static APoint up2;
	static APoint down2;
	
	// collision test vectors for box2
	static AVector left2test;
	static AVector down2test;
	static AVector right2test;
	static AVector up2test;
	
	// circle2circle vector
	static AVector center;
	
	// circle2square vectors;
	static float dist[4];
	static AVector close[4];
	static AVector corner2corner;
	static AVector corner2center;
	static APoint closestPoint;
	static AVector closestVector;
	
	// for eye not moving but being pushed against walls
	static AVector pusher2pushee;
	
	
	/*************************************************
	 * MEMBER VARIABLES
	 *************************************************/
	unsigned int nId;	// should be assigned in program so actors in vector list do not collision detect themselves
	
	// actor position, size, and orientation
	APoint vLocation;	// where am I?
	AVector vForward;	// where am I going?
	AVector vUp;	// which way is up?
	AVector vRight;		// CrossProduct(vForward, vUp)
	float fHalfHeight;	// height of actor
	float fTopY;	// top of actor in y
	float fBotY;	// bottom of actor in y
	float fRotX;	// up and down rotation
	float fRotY;	// left and right rotation

	// collision data
	XZSquare boundingBox;	// for square collisions
	XZCircle boundingCircle;	// for circle collisions
	
	// velocities and speeds
	float fGravity;
	float fTransSpeed;
	float fForwardVelocity;
	float fBackwardVelocity;
	float fUpVelocity;
	float fDownVelocity;
	float fRightVelocity;
	float fLeftVelocity;
	float fRotSpeed;
	float fRotXVelocity;
	float fRotYVelocity;

	// collision data
	bool bCollision;	// general collision between actors
	bool bTopCollision;	// actor collide with block above?
	bool bBotCollision;	// actor collide with actor below?
	bool bInteractCollision;	// actor collide with another actor on same level?
	bool bEdge;	// actor on block edge?
	bool bCorner;	// actor on block corner?	
	int nSquished;	// countdown for actor being squashed between two blocks
	
	// jump data
	bool bCanJump;	// can actor jump?
	int nJumpDelay;	// delay between being able to jump
	
	// hit data
	bool bHit;	// was actor hit by another actor?
	
	// monster hearts
	bool bHeart;	// monster has been killed and turned into a heart
	bool bDelete;
	
	// reset data
	float fXLoc;
	float fYLoc;
	float fZLoc;
	float fXRot;
	float fYRot;
	
	
	/* CONSTRUCTORS & DESTRUCTOR */
	AActor(void)	// default constructor
	{
		nId = 123456789;	// means not initialized
		
		// actor position, size, and orientation
		vLocation.Set(0.0f, 0.0f, 0.0f, 1.0f);	// start at origin, also the center of actor
		vForward.Set(0.0f, 0.0f, -1.0f);	// OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		fHalfHeight = 0.0f;
		fTopY = 0.0f;
		fBotY = 0.0f;
		fRotX = 0.0f;
		fRotY = 0.0f;
		
		// collision data
		boundingBox.Init(vLocation, vForward, vRight, 0.0f);
		boundingCircle.Init(vLocation, 0.0f);
		
		// velocities and speeds
		fGravity = GRAVITY;
		fTransSpeed = 0.0f;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		fRotSpeed = 0.0f;
		fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;

		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
	}
	

//	AActor(const AActor& a)
//	{
	//	use default copy constructor (deep copying unnecessary) and std::vector uses copy constructor
//	}
	
//	AActor& operator = (AActor& a)
//	{
	//	use default assignnment operator
//	}
	
	virtual ~AActor(void)	// since AActor is a base class, if we ever have a base object pointer/reference refering to a derived object type, we want to dynamic bind a call to the destructor of the object type (derived)! not to the pointer type (base). compiler uses static binding to pointer type (base) by default since it is known at compile time so we make this destructor virtual to solve that
	{
		// silent do-nothing destructor
	}
	
	virtual void Init(int monsterId, float xloc, float yloc, float zloc, float height, float radius, float transSpeed, float rotSpeed, float xrot, float yrot) { } // function signature of a_monster.h
	
	virtual void Init(int blockId, float xloc, float yloc, float zloc, float height, float boxSideLen, float radius, float xrot, float yrot) { } // function signature of a_block.h
	
	/* SETTERS */
	void LoadIdentity(void)	// simulate glLoadIdentity() -- reset unit vectors and bound angles (leave location alone) before rendering to screen
	{
		// reset forward and up vectors (right too) but leave location alone
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// set up
		CrossProduct(vRight, vForward, vUp);	// get right from forward & up
		Norm(vRight);	// make sure right is an accurate unit vector
		
		// bound x rotation angle between [0, 359] to work with sinLut/cosLut
		if ( fRotX < 0.0f )
			fRotX += 360.0f;
		if ( fRotX > 360.0f )
			fRotX -= 360.0f;
		
		// bound y rotation angle between [0, 359] to work with sinLut/cosLut
		if ( fRotY < 0.0f )
			fRotY += 360.0f;
		if ( fRotY > 360.0f )
			fRotY -= 360.0f;
	}
	
	void SetGravity(float g)
	{
		fGravity = g;
	}
	
	/* GETTERS */
	float* GetMatrix(void)	// get transformation matrix from right,up,forward,location vectors
	{
		// normalize up and forward unit vectors
		Norm(vUp);
		Norm(vForward);
		
		// set right unit vector from forward and up vectors 
		CrossProduct(vRight, vForward, vUp);
		Norm(vRight);
		
		// reset temp matrix
		m.Reset();
		
		// set right vector
		m[0] = vRight[0];
		m[1] = vRight[1];
		m[2] = vRight[2];
		//m[3] = 0.0f;
		
		// set up unit vector
		m[4] = vUp[0];
		m[5] = vUp[1];
		m[6] = vUp[2];
		//m[7] = 0.0f;
		
		// set forward unit vector
		m[8] = vForward[0];
		m[9] = vForward[1];
		m[10] = vForward[2];
		//m[11] = 0.0f;
		
		// set location
		m[12] = vLocation[0];
		m[13] = vLocation[1];
		m[14] = vLocation[2];
		//m[15] = 1.0f;
		
		return m.GetMatrix();
	}
	
	/* TRANSLATE LOCAL COORD SYSTEM */
	void MoveRightX(float x)
	{
		vLocation[0] += vRight[0]*x;
	}
	
	void MoveRightY(float x)
	{
		vLocation[1] += vRight[1]*x;
	}
	
	void MoveRightZ(float x)
	{
		vLocation[2] += vRight[2]*x;
	}
	
	void MoveUpX(float y)
	{
		vLocation[0] += vUp[0]*y;
	}
	
	void MoveUpY(float y)
	{
		vLocation[1] += vUp[1]*y;
	}
	
	void MoveUpZ(float y)
	{
		vLocation[2] += vUp[2]*y;
	}
	
	void MoveForwardX(float z)
	{
		vLocation[0] += vForward[0]*z;
	}
	
	void MoveForwardY(float z)
	{
		vLocation[1] += vForward[1]*z;
	}
	
	void MoveForwardZ(float z)
	{
		vLocation[2] += vForward[2]*z;
	}
	
	void MoveRightX(const AVector& right, float x)
	{
		vLocation[0] += right[0]*x;
	}
	
	void MoveRightY(const AVector& right, float x)
	{
		vLocation[1] += right[1]*x;
	}
	
	void MoveRightZ(const AVector& right, float x)
	{
		vLocation[2] += right[2]*x;
	}
	
	void MoveUpX(const AVector& up, float y)
	{
		vLocation[0] += up[0]*y;
	}
	
	void MoveUpY(const AVector& up, float y)
	{
		vLocation[1] += up[1]*y;
	}
	
	void MoveUpZ(const AVector& up, float y)
	{
		vLocation[2] += up[2]*y;
	}
	
	void MoveForwardX(const AVector& forward, float z)
	{
		vLocation[0] += forward[0]*z;
	}
	
	void MoveForwardY(const AVector& forward, float z)
	{
		vLocation[1] += forward[1]*z;
	}
	
	void MoveForwardZ(const AVector& forward, float z)
	{
		vLocation[2] += forward[2]*z;
	}
	
	void MoveDirectionX(const AVector& direction, float x)
	{
		vLocation[0] += direction[0]*x;
	}
	
	void MoveDirectionY(const AVector& direction, float y)
	{
		vLocation[1] += direction[1]*y;
	}
	
	void MoveDirectionZ(const AVector& direction, float z)
	{
		vLocation[2] += direction[2]*z;
	}
	
	/* ROTATE LOCAL COORD SYSTEM */
	void RotateX(float angle)
	{
		// reset temp matrix
		m.Reset();
		
		// turn into rotation matrix
		m.RotateX(angle);
		
		// rotate forward and up unit vectors
		Multiply(vTemp, m, vForward);
		vForward = vTemp;
		Multiply(vTemp, m, vUp);
		vUp = vTemp;
		Norm(vForward);
		Norm(vUp);
	}
	
	void RotateY(float angle)
	{
		// reset temp matrix
		m.Reset();
		
		// turn into rotation matrix
		m.RotateY(angle);
		
		// rotate forward unit vector (up not affected since on y)
		Multiply(vTemp, m, vForward);
		vForward = vTemp;
		Multiply(vTemp, m, vRight);
		vRight = vTemp;
		Norm(vForward);
		Norm(vRight);
	}
	
	void RotateZ(float angle)
	{
		// reset temp matrix
		m.Reset();
		
		// turn into rotation matrix
		m.RotateZ(angle);
		
		// rotate up unit vector (forward not affected since on z)
		Multiply(vTemp, m, vUp);
		vUp = vTemp;
		Multiply(vTemp, m, vRight);
		vRight = vTemp;
		Norm(vUp);
		Norm(vRight);
	}
	
	void Rotatef(float angle, float x, float y, float z)
	{
		if ( x==1.0f && y==0.0f && z==0.0f )
			RotateX(angle);
		else if ( x==0.0f && y==1.0f && z==0.0f )
			RotateY(angle);
		else if ( x==0.0f && y==0.0f && z==1.0f )
			RotateZ(angle);
	}
	

	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 CIRCLE COLLISION DETECTION: test for circle to circle collisions on xz plane: block
	 *|		outter bounding circles, eye, monsters
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Circle2CircleCollision(const XZCircle& c1, const XZCircle& c2)
	{
		Subtract(center, c2.center, c1.center);
		
		// sqrt(dotproduct(vA,vA)) = length of vA ||A||
		float distance = DotProduct(center, center);
		distance = 1.0f / InvSqrt(distance);	// distance between both centers
		
		// if distance between two circle center points is less than the two circle radii, we have a collision!
		if ( distance < (c1.rad + c2.rad) )
			return true;
		else
			// otherwise, no collisions
			return false;
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 SQUARE COLLISION DETECTION: test for circle to square collision on xz plane:
	 *|		eye to block, monster to block
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Circle2SquareCollision(const XZCircle& c, const XZSquare& box)
	{
		/*************************************************************
		 * TEST CIRCLE INSIDE BOX: first test if center of circle is within box by testing
		 *	center against square's 4 directional arrows
		 *************************************************************/
		// setup box's directional vectors
		Subtract(l2d, box[2], box[0]);
		Subtract(d2r, box[3], box[2]);
		Subtract(r2u, box[1], box[3]);
		Subtract(u2l, box[0], box[1]);
		
		// setup vectors from directional vector start points to center
		Subtract(close[0], c.center, box[0]);
		Subtract(close[1], c.center, box[2]);	// start points keep in line with above
		Subtract(close[2], c.center, box[3]);
		Subtract(close[3], c.center, box[1]);
		
		
		// if the dot product of center with all 4 directional vectors is positive, then circle's center is within box's square
		if ( DotProduct(close[0], l2d) >= 0.0f &&
			DotProduct(close[1], d2r) >= 0.0f &&
			DotProduct(close[2], r2u) >= 0.0f &&
			DotProduct(close[3], u2l) >= 0.0f )
		{
			return true;
		}
		
		/*************************************************************
		 * TEST IF CIRCLE EDGE TOUCHES BOX EDGE: to test if the edges touch, find the point on the
		 *	square closest to the circle and test if that point is outside the square (corner test)
		 *	or if that point is inside the square (point on square to circle center vs. circle radius test)
		 *************************************************************/
		// find the two closest corners on the square and a make a corner2corner vector between those two points		
		Subtract(close[0], c.center, box[0]);
		dist[0] = DotProduct(close[0], close[0]);
		Subtract(close[1], c.center, box[1]);
		dist[1] = DotProduct(close[1], close[1]);
		Subtract(close[2], c.center, box[2]);
		dist[2] = DotProduct(close[2], close[2]);
		Subtract(close[3], c.center, box[3]);
		dist[3] = DotProduct(close[3], close[3]);
		
		// values to hold first closest corner
		float min = dist[0];
		int nMin = 0;
		
		if ( dist[1] < min )
		{
			min = dist[1];
			nMin = 1;
		}
		
		if ( dist[2] < min )
		{
			min = dist[2];
			nMin = 2;
		}
		
		if ( dist[3] < min )
		{
			min = dist[3];
			nMin = 3;
		}
		
		// values to hold next closest corner
		float min2;
		int nMin2 = 0;
		
		switch ( nMin )
		{
			case 0:
				min2 = dist[1];
				nMin2 = 1;
				
				if ( dist[2] < min2 )
				{
					min2 = dist[2];
					nMin2 = 2;
				}
				
				if ( dist[3] < min2 )
				{
					min2 = dist[3];
					nMin2 = 3;
				}
				
				break;
			case 1:
				min2 = dist[0];
				nMin2 = 0;
				
				if ( dist[2] < min2 )
				{
					min2 = dist[2];
					nMin2 = 2;
				}
				
				if ( dist[3] < min2 )
				{
					min2 = dist[3];
					nMin2 = 3;
				}
				break;
			case 2:
				min2 = dist[0];
				nMin2 = 0;
				
				if ( dist[1] < min2 )
				{
					min2 = dist[1];
					nMin2 = 1;
				}
				
				if ( dist[3] < min2 )
				{
					min2 = dist[3];
					nMin2 = 3;
				}
				break;
			case 3:
				min2 = dist[0];
				nMin2 = 0;
				
				if ( dist[1] < min2 )
				{
					min2 = dist[1];
					nMin2 = 1;
				}
				
				if ( dist[2] < min2 )
				{
					min2 = dist[2];
					nMin2 = 2;
				}
				break;
				
			default:
				break;
		}
		
		// make a corner2corner vector between the two closest points
		Subtract(corner2corner, box[nMin2], box[nMin]);
		
		// make a corner2center vector starting from the same square point to the circles center
		Subtract(corner2center, c.center, box[nMin]);
		
		// project corner2center vector onto corner2corner vector
		float corner2centerDist = DotProduct(corner2center, corner2center); // get length of corner2center distance
		corner2centerDist = 1.0f / InvSqrt(corner2centerDist);
		float corner2cornerDist = DotProduct(corner2corner, corner2corner);
		corner2cornerDist = 1.0f / InvSqrt(corner2cornerDist);
		float ABmag = corner2centerDist*corner2cornerDist;

		// cos(theta) = dot(A,B) / ||A||*||B||
		float angleBetween = DotProduct(corner2center, corner2corner); // get angle between vectors
		angleBetween /= ABmag;
		
		// if we have -costheta (i.e. closest point on square lies outside of square), then we test if circle's center to nearest corner is less than radius
		if ( angleBetween < 0 )
		{
			// test how far center is from that corner
			Subtract(corner2center, c.center, box[nMin]);
			float distance = 1.0f / InvSqrt(DotProduct(corner2center, corner2center));
			
			// if distance between closest corner and circle center is less than circle radius, we have a hit at -costheta
			if ( distance < c.rad )
			{
				bCorner = true;
				return true;
			}
		}
		// if costheta positive (i.e. closest point on square lies inside square), we can use the closest point on square
		else
		{	
			Norm(corner2corner);	// make projected vector new direction a unit vector
			
			// AprojB = ||A||cos(theta)*uB
			Multiply(closestPoint, corner2corner, corner2centerDist*angleBetween);
			
			// make closest point the point on the square
			Add(closestPoint, closestPoint, box[nMin]);
			
			// get the vector from the closest point on square to the circle's center
			Subtract(closestVector, closestPoint, c.center);
			
			// sqrt(dotproduct(vA,vA)) = length of vA ||A||
			float distance = DotProduct(closestVector, closestVector);
			distance = 1.0f / InvSqrt(distance);	// want half the distance
			
			// if distance between two circle center points is more than the two circle radii, we have a collision!
			if ( distance < c.rad )
			{
				bEdge = true;
				return true;
			}
		}
		
		// otherwise, no collisions
		return false;
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 SQUARE COLLISION DETECTION: test for square to square collision on xz plane:
	 *|		block to block
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Square2SquareCollision(const XZSquare& box1, const XZSquare& box2)
	{
		/*************************************************************
		 * TEST BOX1 INSIDE BOX2: first test if center of box1 is within box2 by testing
		 *	center against square's 4 directional arrows. if box1 is within box2 then vice versa
		 *	is also true, so we only need to test this case.	
		 *************************************************************/
		// setup box's directional vectors
		Subtract(l2d, box2[2], box2[0]);
		Subtract(d2r, box2[3], box2[2]);
		Subtract(r2u, box2[1], box2[3]);
		Subtract(u2l, box2[0], box2[1]);
		
		// setup vectors from directional vector start points to center
		Subtract(close[0], box1.center, box2[0]);
		Subtract(close[1], box1.center, box2[2]);	// start points keep in line with above
		Subtract(close[2], box1.center, box2[3]);
		Subtract(close[3], box1.center, box2[1]);
		
		
		// if the dot product of center with all 4 directional vectors is positive, then circle's center is within box's square
		if ( DotProduct(close[0], l2d) >= 0.0f &&
			DotProduct(close[1], d2r) >= 0.0f &&
			DotProduct(close[2], r2u) >= 0.0f &&
			DotProduct(close[3], u2l) >= 0.0f )
		{
			return true;
		}
		
		/**************************************************
		 * SET COLLISION POINTS: set left, up, down, right points for both squares
		 **************************************************/
		int leftest; // leftest will be used to find the leftmost vertex of each box
		
		// find leftmost vertex of your box
		left = box1[0];
		leftest = 0;
		
		// find far left vertex
		if ( box1[1][0] < left[0] )
		{
			left = box1[1];
			leftest = 1;
		}
		if ( box1[2][0] < left[0] )
		{
			left = box1[2];
			leftest = 2;
		}
		if ( box1[3][0] < left[0] )
		{
			left = box1[3];
			leftest = 3;
		}
		
		// assign all other vertices based on leftmost vertex
		switch (leftest)
		{
			case 0:
				//left = box1[0];
				up = box1[1];
				down = box1[2];
				right = box1[3];
				break;
			case 1:
				//left = box1[1];
				up = box1[3];
				down = box1[0];
				right = box1[2];
				break;
			case 2:
				//left = box1[2];
				up = box1[0];
				down = box1[3];
				right = box1[1];
				break;
			case 3:
				//left = box1[3];
				up = box1[2];
				down = box1[1];
				right = box1[0];
				break;
				
			default:
				break;
		}
		
		// find leftmost vertex of other box
		left2 = box2[0];
		leftest = 0;
		
		// find far left vertex
		if ( box2[1][0] < left2[0] )
		{
			left2 = box2[1];
			leftest = 1;
		}
		if ( box2[2][0] < left2[0] )
		{
			left2 = box2[2];
			leftest = 2;
		}
		if ( box2[3][0] < left2[0] )
		{
			left2 = box2[3];
			leftest = 3;
		}
		
		// assign all other vertices based on leftmost vertex
		switch (leftest)
		{
			case 0:
				//left2 = box2[0];
				up2 = box2[1];
				down2 = box2[2];
				right2 = box2[3];
				break;
			case 1:
				//left2 = box2[1];
				up2 = box2[3];
				down2 = box2[0];
				right2 = box2[2];
				break;
			case 2:
				//left2 = box2[2];
				up2 = box2[0];
				down2 = box2[3];
				right2 = box2[1];
				break;
			case 3:
				//left2 = box2[3];
				up2 = box2[2];
				down2 = box2[1];
				right2 = box2[0];
				break;
				
			default:
				break;
		}
		
		/**************************************************
		 * SET COLLISION VECTORS AND TEST IF ANY OF BOX2's POINTS ARE IN BOX1: for each of box2's
		 * points, dot product with all of box1's directional vectors. if all return a cosine
		 * value [0, 90] then that point is inside box1 (i.e. if a negative is returned, then the
		 * test vector's projection to one of the directional vectors is facing the opposite
		 * direction of that directional vector)
		 **************************************************/
		// setup box1's directional vectors
		Subtract(l2d, down, left);
		Subtract(d2r, right, down);
		Subtract(r2u, up, right);
		Subtract(u2l, left, up);
		
		// setup test vectors from start points of box1's directional vectors to test box2's left2 vector
		Subtract(left2test, left2, left);
		Subtract(down2test, left2, down);
		Subtract(right2test, left2, right);
		Subtract(up2test, left2, up);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box2's left point is in box1!
			return true;
		}
		
		// setup test vectors from start points of box1's directional vectors to test box2's down2 vector
		Subtract(left2test, down2, left);
		Subtract(down2test, down2, down);
		Subtract(right2test, down2, right);
		Subtract(up2test, down2, up);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0 ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box2's down point is in box1!
			return true;
		}
		
		// setup test vectors from start points of box1's directional vectors to test box2's right2 vector
		Subtract(left2test, right2, left);
		Subtract(down2test, right2, down);
		Subtract(right2test, right2, right);
		Subtract(up2test, right2, up);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box2's right point is in box1!
			return true;
		}
		
		// setup test vectors from start points of box1's directional vectors to test box2's up2 vector
		Subtract(left2test, up2, left);
		Subtract(down2test, up2, down);
		Subtract(right2test, up2, right);
		Subtract(up2test, up2, up);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box2's up point is in box1!
			return true;
		}
		
		/**************************************************
		 * SET COLLISION VECTORS AND TEST IF ANY OF BOX1's POINTS ARE IN BOX2: go through process
		 * again but this time test if any of box1's points are in box2.
		 **************************************************/
		// setup box2's directional vectors
		Subtract(l2d, down2, left2);
		Subtract(d2r, right2, down2);
		Subtract(r2u, up2, right2);
		Subtract(u2l, left2, up2);
		
		// setup test vectors from start points of box2's directional vectors to test box1's left vector
		Subtract(left2test, left, left2);
		Subtract(down2test, left, down2);
		Subtract(right2test, left, right2);
		Subtract(up2test, left, up2);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box1's left point is in box2!
			return true;
		}
		
		// setup test vectors from start points of box2's directional vectors to test box1's down vector
		Subtract(left2test, down, left2);
		Subtract(down2test, down, down2);
		Subtract(right2test, down, right2);
		Subtract(up2test, down, up2);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box1's down point is in box2!
			return true;
		}
		
		// setup test vectors from start points of box2's directional vectors to test box1's right vector
		Subtract(left2test, right, left2);
		Subtract(down2test, right, down2);
		Subtract(right2test, right, right2);
		Subtract(up2test, right, up2);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box1's right point is in box2!
			return true;
		}
		
		// setup test vectors from start points of box2's directional vectors to test box1's up2 vector
		Subtract(left2test, up, left2);
		Subtract(down2test, up, down2);
		Subtract(right2test, up, right2);
		Subtract(up2test, up, up2);
		
		if (
			(DotProduct(l2d, left2test) > 0.0f ) &&
			(DotProduct(d2r, down2test) > 0.0f ) &&
			(DotProduct(r2u, right2test) > 0.0f ) &&
			(DotProduct(u2l, up2test) > 0.0f ))
		{
			// if true, box1's up point is in box2!
			return true;
		}

		// otherwise there were no collisions
		return false;
	}
	
	/* VIRTUAL FUNCTIONS */
	virtual void Reset(void)
	{
		// reset actor to starting position
	}
	
	virtual void Init(void)
	{
		// initialize actor location, size, orientation and variables
	}
	
	virtual void ProcessEvents(SDL_Event& event)
	{
		// change velocities based on events
	}
	
	virtual void ProcessLogic(void)
	{
		// rotate actor by rotations
		// move actor by velocities
		// update actor top,bot and collision boundaries
		
		// check actor collisions against on other actors in roty, y, x, z
		
		// move back by velocity if collision detected
		// update top,bot and collision boundaries
		// check collisions again and move again... repeat until there are no collisions for this frame (if actor is stuck, the decremented nSquish counter will reset the actor to initial values
	}
	
	virtual	void ProcessLogic(const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators) { }	// function signature of a_monster.h
	virtual void ProcessLogic(AActor* eye, const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators, std::vector<AActor*>& monsters) { } // function signature of a_block.h
	
	virtual void Display(GLuint lDrawing)
	{
		// eye:
		//	rotx
		//	draw eye
		//	roty
		//	translate
		
		// other actors:
		//	rotx
		//	roty
		//	translate
		//	draw actor
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const AActor& a)
	{
		os << "** actor **: " << a.nId << std::endl;
		os << "\tvLocation: " << a.vLocation << std::endl;
		os << "Velocities (x, y, z): (" << a.fRightVelocity + a.fLeftVelocity << "," << a.fUpVelocity + a.fDownVelocity << "," << a.fForwardVelocity + a.fBackwardVelocity << ") gravity = " << a.fGravity << "\n";
		os << "Rot (x, y): (" << a.fRotX << ", " << a.fRotY << ")\n";
		os << "boundingCircle:\n";
		os << a.boundingCircle << std::endl;
		os << "bCollision: " << a.bCollision << std::endl;
		os << "bTopCollision: " << a.bTopCollision << std::endl;
		os << "bBotCollision: " << a.bBotCollision << std::endl;
		os << "bInteractionCollision: " << a.bInteractCollision << std::endl;
		os << "bEdge: " << a.bEdge << std::endl;
		os << "bCorner: " << a.bCorner << std::endl;
		os << "nSquished: " << a.nSquished << std::endl;
		os << "bCanJump: " << a.bCanJump << std::endl;
		os << "nJumpDelay: " << a.nJumpDelay << std::endl;
		os << "bHit: " << a.bHit << std::endl;
		os << "fTopY: " << a.fTopY << ", fBotY: " << a.fBotY << std::endl;
		os << "bHeart: " << a.bHeart << std::endl;
		os << "bDelete: " << a.bDelete << std::endl;
		
		return os;
	}
	
	
	
	
	


	
	
}; // end of class AActor


#endif // A_ACTOR_H_





