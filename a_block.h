//
//  a_block.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-04.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_BLOCK_H_
#define A_BLOCK_H_

#include "a_actor.h"

// init values for small blocks
const float SMALL_BLOCK_SIDE_LENGTH = 1.0f;
const float SMALL_BLOCK_RADIUS = SMALL_BLOCK_SIDE_LENGTH * SIN45 + ERROR_RANGE_SMALL;
const float SMALL_BLOCK_HEIGHT = 1.0f;

// init values for big blocks
const float BIG_BLOCK_SIDE_LENGTH = 4.0f;
const float BIG_BLOCK_RADIUS = BIG_BLOCK_SIDE_LENGTH * SIN45 + ERROR_RANGE_SMALL;
const float BIG_BLOCK_HEIGHT = 1.0f;

// init values for huge blocks
const float HUGE_BLOCK_SIDE_LENGTH = 7.0f;
const float HUGE_BLOCK_RADIUS = HUGE_BLOCK_SIDE_LENGTH * SIN45 + ERROR_RANGE_SMALL;
const float HUGE_BLOCK_HEIGHT = 1.0f;




/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	ABlock: static blocks that make up the ground terrain and island terrain.
 *| 
 *|	Class Hierarchy:
 *|
 *|		- AActor:
 *|
 *|			- ABlock: a static block that makes up the ground terrain and island terrain.
 *|
 *|				- AElevator: moving blocks that make up moving platforms within game
 *|
 *|					- ABoat: a special kind of elevator: moving blocks that  player controlled transport vessels
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class ABlock: public AActor
{
private:
	
public:
	ABlock(void) : AActor()
	{
		
	}
	
	virtual ~ABlock(void)
	{
		
	}
	
	/* VIRTUAL FUNCTION REDEFINITIONS */
	virtual void Init(int blockId, float xloc, float yloc, float zloc, float height, float boxSideLen, float radius, float xrot, float yrot)
	{
		nId = blockId;
		
		// block position, size, and orientation
		vLocation.Set(xloc, yloc, zloc, 1.0f);
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = xrot;
		fRotY = yrot;
		
		// collision data
		boundingBox.Init(vLocation, vForward, vRight, boxSideLen);
		boundingCircle.Init(vLocation, radius);
		
		// velocities and speeds
		fGravity = GRAVITY;
		//fTransSpeed = 0.0f;
		//fForwardVelocity = 0.0f;
		//fBackwardVelocity = 0.0f;
		//fUpVelocity = 0.0f;
		//fDownVelocity = 0.0f;
		//fRightVelocity = 0.0f;
		//fLeftVelocity = 0.0f;
		//fRotSpeed = 0.0f;
		//fRotXVelocity = 0.0f;
		//fRotYVelocity = 0.0f;
		
		// collision data
		//bCollision = false;
		//bTopCollision = false;
		//bBotCollision = false;
		//bInteractCollision = false;
		//bEdge = false;
		//bCorner = false;
		//nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		//bCanJump = false;
		//nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		//bHit = false;
	}
	
	virtual void Reset(void)
	{
		// blocks are stationary and do not detect collisions so they should never reset
	}
	
	virtual void ProcessEvents(SDL_Event& event)
	{
		// blocks themselves are static and do not move in the scene
	}
	
	virtual void ProcessLogic(AActor* eye, const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators, std::vector<AActor*>& monsters)
	{	
		// blocks don't actually move, so no logic to process (add gravity to some blocks later?)
	}
	
	void Display(GLuint lDrawing)
	{
		glPushMatrix(); // draw block
		{
			// translate drawer to block's location
			glTranslatef(vLocation[0], vLocation[1], vLocation[2]);
			
			// rotate drawer in y before drawing block (local rotation)
			glRotatef(fRotY, 0.0f, 1.0f, 0.0f);
			
			// draw block
			glCallList(lDrawing);	
		}
		glPopMatrix(); // end draw block
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const ABlock& b)
	{
		os << "** block **: " << b.nId << std::endl;
		os << "\tvLocation: " << b.vLocation << std::endl;
		os << "fGravity: " << b.fGravity << std::endl;
		os << "Rot (x, y): (" << b.fRotX << ", " << b.fRotY << ")\n";
		os << "boundingBox:\n";
		os << b.boundingBox << std::endl;
		os << "boundingCircle:\n";
		os << b.boundingCircle << std::endl;
		os << "fTopY: " << b.fTopY << ", fBotY: " << b.fBotY << std::endl;
		
		return os;
	}
	
	
};	// end of class ABlock



#endif	// A_BLOCK_H_





