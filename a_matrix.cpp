
//
//  a_matrix.cpp
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_matrix.h"
#include <cmath>	// for sin and cos



/**********************************************
 * INIT STATICS (should only initialize once on first object creation)
 **********************************************/
AMatrix AMatrix::temp = AMatrix();

// use in addition to temp (when temp currently in use and need another temporary matrix)
float AMatrix::a0 = 0.0f;
float AMatrix::a1 = 0.0f;
float AMatrix::a2 = 0.0f;
float AMatrix::a3 = 0.0f;
float AMatrix::a4 = 0.0f;
float AMatrix::a5 = 0.0f;
float AMatrix::a6 = 0.0f;
float AMatrix::a7 = 0.0f;
float AMatrix::a8 = 0.0f;
float AMatrix::a9 = 0.0f;
float AMatrix::a10 = 0.0f;
float AMatrix::a11 = 0.0f;
float AMatrix::a12 = 0.0f;
float AMatrix::a13 = 0.0f;
float AMatrix::a14 = 0.0f;
float AMatrix::a15 = 0.0f;

// arrays to hold sin/cos lookup tables (must call BuildSinCosLut() before using AMatrix class!)
static float sinLut[360];
static float cosLut[360];
const float RAD_TO_DEGREE = 57.29577951;


/**********************************************
 * CONSTRUCTORS & DESTRUCTORS
 **********************************************/
AMatrix::AMatrix(void)
{
	f[0]=1.0f; f[4]=0.0f; f[8]=0.0f; f[12]=0.0f;
	f[1]=0.0f; f[5]=1.0f; f[9]=0.0f; f[13]=0.0f;
	f[2]=0.0f; f[6]=0.0f; f[10]=1.0f; f[14]=0.0f;
	f[3]=0.0f; f[7]=0.0f; f[11]=0.0f; f[15]=1.0f;
}

AMatrix::AMatrix(float m0, float m1, float m2, float m3, float m4, float m5, float m6, float m7, float m8, float m9, float m10, float m11, float m12, float m13, float m14, float m15)
{
	f[0]=m0; f[4]=m4; f[8]=m8; f[12]=m12;
	f[1]=m1; f[5]=m5; f[9]=m9; f[13]=m13;
	f[2]=m2; f[6]=m6; f[10]=m10; f[14]=m14;
	f[3]=m3; f[7]=m7; f[11]=m11; f[15]=m15;
}

AMatrix::AMatrix(const float m[16])
{
	f[0]=m[0]; f[4]=m[4]; f[8]=m[8]; f[12]=m[12];
	f[1]=m[1]; f[5]=m[5]; f[9]=m[9]; f[13]=m[13];
	f[2]=m[2]; f[6]=m[6]; f[10]=m[10]; f[14]=m[14];
	f[3]=m[3]; f[7]=m[7]; f[11]=m[11]; f[15]=m[15];
}

AMatrix::AMatrix(const AMatrix& copy)
{
	f[0]=copy.f[0]; f[4]=copy.f[4]; f[8]=copy.f[8]; f[12]=copy.f[12];
	f[1]=copy.f[1]; f[5]=copy.f[5]; f[9]=copy.f[9]; f[13]=copy.f[13];
	f[2]=copy.f[2]; f[6]=copy.f[6]; f[10]=copy.f[10]; f[14]=copy.f[14];
	f[3]=copy.f[3]; f[7]=copy.f[7]; f[11]=copy.f[11]; f[15]=copy.f[15];
}

AMatrix::AMatrix(const AVector& x, const AVector& y, const AVector& z, const AVector& translation)
{
	f[0]=x[0]; f[4]=y[0]; f[8]=z[0]; f[12]=translation[0];
	f[1]=x[1]; f[5]=y[1]; f[9]=z[1]; f[13]=translation[1];
	f[2]=x[2]; f[6]=y[2]; f[10]=z[2]; f[14]=translation[2];
	f[3]=x[3]; f[7]=y[3]; f[11]=z[3]; f[15]=translation[3];
}

AMatrix::~AMatrix(void)
{
	//	std::cerr << "*\t destroying:\n" << *this << "\n... done!" << std::endl;
}



/**********************************************
 * SETTERS
 **********************************************/
void AMatrix::Identity(void)
{
	f[0]=1.0f; f[4]=0.0f; f[8]=0.0f; f[12]=0.0f;
	f[1]=0.0f; f[5]=1.0f; f[9]=0.0f; f[13]=0.0f;
	f[2]=0.0f; f[6]=0.0f; f[10]=1.0f; f[14]=0.0f;
	f[3]=0.0f; f[7]=0.0f; f[11]=0.0f; f[15]=1.0f;
}

void AMatrix::Reset(void)	// same as SetIdentity(), just different language
{
	f[0]=1.0f; f[4]=0.0f; f[8]=0.0f; f[12]=0.0f;
	f[1]=0.0f; f[5]=1.0f; f[9]=0.0f; f[13]=0.0f;
	f[2]=0.0f; f[6]=0.0f; f[10]=1.0f; f[14]=0.0f;
	f[3]=0.0f; f[7]=0.0f; f[11]=0.0f; f[15]=1.0f;
}

void AMatrix::Transpose(void)
{
	Swap(f[1], f[4]);
	Swap(f[2], f[8]);
	Swap(f[3], f[12]);
	Swap(f[6], f[9]);
	Swap(f[7], f[13]);
	Swap(f[11], f[14]);
}

void AMatrix::Zero(void)
{
	f[0]=0.0f; f[4]=0.0f; f[8]=0.0f; f[12]=0.0f;
	f[1]=0.0f; f[5]=0.0f; f[9]=0.0f; f[13]=0.0f;
	f[2]=0.0f; f[6]=0.0f; f[10]=0.0f; f[14]=0.0f;
	f[3]=0.0f; f[7]=0.0f; f[11]=0.0f; f[15]=0.0f;
}



/**********************************************
 * GETTERS
 **********************************************/
AVector AMatrix::GetTranslation(void)	// could be faster but don't really use
{
	return AVector(f[12], f[13], f[14], f[15]);
}

AMatrix AMatrix::GetInverse(void)
{
	float fInv[16];
	
	if ( GenerateInverseMatrix(fInv, f) )
		return AMatrix(fInv);
	else
	{
		std::cerr << "Error getting matrix inverse! Returning zero matrix" << std::endl;
		return AMatrix(0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0,
					   0, 0, 0, 0);
	}
}

float AMatrix::GetDeterminant(const float m[16])
{
	return
	m[12]*m[9]*m[6]*m[3]-
	m[8]*m[13]*m[6]*m[3]-
	m[12]*m[5]*m[10]*m[3]+
	m[4]*m[13]*m[10]*m[3]+
	m[8]*m[5]*m[14]*m[3]-
	m[4]*m[9]*m[14]*m[3]-
	m[12]*m[9]*m[2]*m[7]+
	m[8]*m[13]*m[2]*m[7]+
	m[12]*m[1]*m[10]*m[7]-
	m[0]*m[13]*m[10]*m[7]-
	m[8]*m[1]*m[14]*m[7]+
	m[0]*m[9]*m[14]*m[7]+
	m[12]*m[5]*m[2]*m[11]-
	m[4]*m[13]*m[2]*m[11]-
	m[12]*m[1]*m[6]*m[11]+
	m[0]*m[13]*m[6]*m[11]+
	m[4]*m[1]*m[14]*m[11]-
	m[0]*m[5]*m[14]*m[11]-
	m[8]*m[5]*m[2]*m[15]+
	m[4]*m[9]*m[2]*m[15]+
	m[8]*m[1]*m[6]*m[15]-
	m[0]*m[9]*m[6]*m[15]-
	m[4]*m[1]*m[10]*m[15]+
	m[0]*m[5]*m[10]*m[15];
}

bool AMatrix::GenerateInverseMatrix(float i[16], const float m[16])
{
	// calculate determinant
	float x = GetDeterminant(m);
	
	if ( x == 0 )
		return false;		// determinant cannot be 0!
	
	i[0]= (-m[13]*m[10]*m[7] +m[9]*m[14]*m[7] +m[13]*m[6]*m[11]
		   -m[5]*m[14]*m[11] -m[9]*m[6]*m[15] +m[5]*m[10]*m[15])/x;
	i[4]= ( m[12]*m[10]*m[7] -m[8]*m[14]*m[7] -m[12]*m[6]*m[11]
		   +m[4]*m[14]*m[11] +m[8]*m[6]*m[15] -m[4]*m[10]*m[15])/x;
	i[8]= (-m[12]*m[9]* m[7] +m[8]*m[13]*m[7] +m[12]*m[5]*m[11]
		   -m[4]*m[13]*m[11] -m[8]*m[5]*m[15] +m[4]*m[9]* m[15])/x;
	i[12]=( m[12]*m[9]* m[6] -m[8]*m[13]*m[6] -m[12]*m[5]*m[10]
		   +m[4]*m[13]*m[10] +m[8]*m[5]*m[14] -m[4]*m[9]* m[14])/x;
	i[1]= ( m[13]*m[10]*m[3] -m[9]*m[14]*m[3] -m[13]*m[2]*m[11]
		   +m[1]*m[14]*m[11] +m[9]*m[2]*m[15] -m[1]*m[10]*m[15])/x;
	i[5]= (-m[12]*m[10]*m[3] +m[8]*m[14]*m[3] +m[12]*m[2]*m[11]
		   -m[0]*m[14]*m[11] -m[8]*m[2]*m[15] +m[0]*m[10]*m[15])/x;
	i[9]= ( m[12]*m[9]* m[3] -m[8]*m[13]*m[3] -m[12]*m[1]*m[11]
		   +m[0]*m[13]*m[11] +m[8]*m[1]*m[15] -m[0]*m[9]* m[15])/x;
	i[13]=(-m[12]*m[9]* m[2] +m[8]*m[13]*m[2] +m[12]*m[1]*m[10]
		   -m[0]*m[13]*m[10] -m[8]*m[1]*m[14] +m[0]*m[9]* m[14])/x;
	i[2]= (-m[13]*m[6]* m[3] +m[5]*m[14]*m[3] +m[13]*m[2]*m[7]
		   -m[1]*m[14]*m[7] -m[5]*m[2]*m[15] +m[1]*m[6]* m[15])/x;
	i[6]= ( m[12]*m[6]* m[3] -m[4]*m[14]*m[3] -m[12]*m[2]*m[7]
		   +m[0]*m[14]*m[7] +m[4]*m[2]*m[15] -m[0]*m[6]* m[15])/x;
	i[10]=(-m[12]*m[5]* m[3] +m[4]*m[13]*m[3] +m[12]*m[1]*m[7]
		   -m[0]*m[13]*m[7] -m[4]*m[1]*m[15] +m[0]*m[5]* m[15])/x;
	i[14]=( m[12]*m[5]* m[2] -m[4]*m[13]*m[2] -m[12]*m[1]*m[6]
		   +m[0]*m[13]*m[6] +m[4]*m[1]*m[14] -m[0]*m[5]* m[14])/x;
	i[3]= ( m[9]* m[6]* m[3] -m[5]*m[10]*m[3] -m[9]* m[2]*m[7]
		   +m[1]*m[10]*m[7] +m[5]*m[2]*m[11] -m[1]*m[6]* m[11])/x;
	i[7]= (-m[8]* m[6]* m[3] +m[4]*m[10]*m[3] +m[8]* m[2]*m[7]
		   -m[0]*m[10]*m[7] -m[4]*m[2]*m[11] +m[0]*m[6]* m[11])/x;
	i[11]=( m[8]* m[5]* m[3] -m[4]*m[9]* m[3] -m[8]* m[1]*m[7]
		   +m[0]*m[9]* m[7] +m[4]*m[1]*m[11] -m[0]*m[5]* m[11])/x;
	i[15]=(-m[8]* m[5]* m[2] +m[4]*m[9]* m[2] +m[8]* m[1]*m[6]
		   -m[0]*m[9]* m[6] -m[4]*m[1]*m[10] +m[0]*m[5]* m[10])/x;
	
	return true;
}

float* AMatrix::GetMatrix(void)
{
	return f;
}



/**********************************************
 * ROTATE MATRIX
 **********************************************/
AMatrix& AMatrix::Rotatef(int angle, float u, float v, float w)		// simulate glRotatef(angle, x, y, z)
{
	// math taken from http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/ section 5.2
	// set rotation matrix based on angle and vector uvw
	temp.f[0] = u*u + (1.0f-u*u)*cosLut[angle];
	temp.f[1] = u*v*(1.0f-cosLut[angle]) + w*sinLut[angle];
	temp.f[2] = u*w*(1.0f-cosLut[angle]) - v*sinLut[angle];
	temp.f[3] = 0.0f;
	
	temp.f[4] = u*v*(1.0f-cosLut[angle]) - w*sinLut[angle];
	temp.f[5] = v*v + (1.0f-v*v)*cosLut[angle];
	temp.f[6] = v*w*(1.0f-cosLut[angle]) + u*sinLut[angle];
	temp.f[7] = 0.0f;
	
	temp.f[8] = v*w*(1.0f-cosLut[angle]) + v*sinLut[angle];
	temp.f[9] = v*w*(1.0f-cosLut[angle]) - u*sinLut[angle];
	temp.f[10] = w*w + (1.0f-w*w)*cosLut[angle];
	temp.f[11] = 0.0f;
	
	temp.f[12] = 0.0f;
	temp.f[13] = 0.0f;
	temp.f[14] = 0.0f;
	temp.f[15] = 1.0f;
	
	// multiply this matrix by rotation matrix
	*this *= temp;
	
	return *this;
}


AMatrix& AMatrix::RotateX(int angle)		// right hand rule
{
	// set rotation matrix for x rotation
	temp.f[0]=1.0f; temp.f[4]=0.0f;				temp.f[8]=0.0f;				temp.f[12]=0.0f;
	temp.f[1]=0.0f; temp.f[5]=cosLut[angle];	temp.f[9]=-sinLut[angle];	temp.f[13]=0.0f;
	temp.f[2]=0.0f; temp.f[6]=sinLut[angle];	temp.f[10]=cosLut[angle];	temp.f[14]=0.0f;
	temp.f[3]=0.0f; temp.f[7]=0.0f;				temp.f[11]=0.0f;			temp.f[15]=1.0f;
	
	// multiply this matrix by rotation matrix
	*this *= temp;
	
	return *this;
}

AMatrix& AMatrix::RotateY(int angle)
{
	// set rotation matrix for y rotation
	temp.f[0]=cosLut[angle];	temp.f[4]=0.0f; temp.f[8]=sinLut[angle];	temp.f[12]=0.0f;
	temp.f[1]=0.0f;				temp.f[5]=1.0f; temp.f[9]=0.0f;				temp.f[13]=0.0f;
	temp.f[2]=-sinLut[angle];	temp.f[6]=0.0f; temp.f[10]=cosLut[angle];	temp.f[14]=0.0f;
	temp.f[3]=0.0f;				temp.f[7]=0.0f; temp.f[11]=0.0f;			temp.f[15]=1.0f;
	
	// multiply this matrix by rotation matrix
	*this *= temp;
	
	return *this;
}

AMatrix& AMatrix::RotateZ(int angle)
{
	// set rotation matrix for z rotation
	temp.f[0]=cosLut[angle];	temp.f[4]=-sinLut[angle];	temp.f[8]=0.0f;		temp.f[12]=0.0f;
	temp.f[1]=sinLut[angle];	temp.f[5]=cosLut[angle];	temp.f[9]=0.0f;		temp.f[13]=0.0f;
	temp.f[2]=0.0f;				temp.f[6]=0.0f;				temp.f[10]=1.0f;	temp.f[14]=0.0f;
	temp.f[3]=0.0f;				temp.f[7]=0.0f;				temp.f[11]=0.0f;	temp.f[15]=1.0f;
	
	// multiply this matrix by rotation matrix
	*this *= temp;
	
	return *this;
}



/**********************************************
 * SCALE MATRIX (squish and expand)
 **********************************************/
AMatrix& AMatrix::Scalef(float x, float y, float z)
{
	// set temp to scale matrix
	temp.f[0]=x;	temp.f[4]=0.0f;	temp.f[8]=0.0f;	temp.f[12]=0.0f;
	temp.f[1]=0.0f;	temp.f[5]=y;	temp.f[9]=0.0f;	temp.f[13]=0.0f;
	temp.f[2]=0.0f;	temp.f[6]=0.0f;	temp.f[10]=z;	temp.f[14]=0.0f;
	temp.f[3]=0.0f;	temp.f[7]=0.0f;	temp.f[11]=0.0f;temp.f[15]=1.0f;
	
	// multiply this matrix by scale matrix
	*this *= temp;

	return *this;
}



/**********************************************
 * TRANSLATE MATRIX
 **********************************************/
AMatrix& AMatrix::Translatef(float x, float y,float z)	// simulate glTranslatef(x, y, z)
{
	// set temp to translation matrix
	temp.f[0]=1.0f;	temp.f[4]=0.0f;	temp.f[8]=0.0f;	temp.f[12]=x;
	temp.f[1]=0.0f;	temp.f[5]=1.0f;	temp.f[9]=0.0f;	temp.f[13]=y;
	temp.f[2]=0.0f;	temp.f[6]=0.0f;	temp.f[10]=1.0f;temp.f[14]=z;
	temp.f[3]=0.0f;	temp.f[7]=0.0f;	temp.f[11]=0.0f;temp.f[15]=1.0f;
	
	// multiply this matrix by translation matrix
	*this *= temp;
	
	return *this;
}

AMatrix& AMatrix::Translate(const float x, const float y, const float z)
{
	// set temp to translation matrix
	temp.f[0]=1.0f;	temp.f[4]=0.0f;	temp.f[8]=0.0f;	temp.f[12]=x;
	temp.f[1]=0.0f;	temp.f[5]=1.0f;	temp.f[9]=0.0f;	temp.f[13]=y;
	temp.f[2]=0.0f;	temp.f[6]=0.0f;	temp.f[10]=1.0f;temp.f[14]=z;
	temp.f[3]=0.0f;	temp.f[7]=0.0f;	temp.f[11]=0.0f;temp.f[15]=1.0f;
	
	// multiply this matrix by translation matrix
	*this *= temp;
	
	return *this;
}

AMatrix& AMatrix::Translate(const AVector& v)
{
	// set temp to translation matrix
	temp.f[0]=1.0f;	temp.f[4]=0.0f;	temp.f[8]=0.0f;	temp.f[12]=v[0];
	temp.f[1]=0.0f;	temp.f[5]=1.0f;	temp.f[9]=0.0f;	temp.f[13]=v[1];
	temp.f[2]=0.0f;	temp.f[6]=0.0f;	temp.f[10]=1.0f;temp.f[14]=v[2];
	temp.f[3]=0.0f;	temp.f[7]=0.0f;	temp.f[11]=0.0f;temp.f[15]=1.0f;
	
	// multiply this matrix by translation matrix
	*this *= temp;
	
	return *this;
}



/**********************************************
 * OPERATOR OVERLOADS
 **********************************************/
AMatrix& AMatrix::operator = (const AMatrix& other)
{
	f[0] = other.f[0];
	f[1] = other.f[1];
	f[2] = other.f[2];
	f[3] = other.f[3];
	f[4] = other.f[4];
	f[5] = other.f[5];
	f[6] = other.f[6];
	f[7] = other.f[7];
	f[8] = other.f[8];
	f[9] = other.f[9];
	f[10] = other.f[10];
	f[11] = other.f[11];
	f[12] = other.f[12];
	f[13] = other.f[13];
	f[14] = other.f[14];
	f[15] = other.f[15];	// should always be 1.0f
	
	return *this;
}

bool AMatrix::operator == (const AMatrix& other) const
{
	return (f[0] == other.f[0] && f[1] == other.f[1] && f[2] == other.f[2] && f[3] == other.f[3] &&
			f[4] == other.f[4] && f[5] == other.f[5] && f[6] == other.f[6] && f[7] == other.f[7] &&
			f[8] == other.f[8] && f[9] == other.f[9] && f[10] == other.f[10] && f[11] == other.f[11] &&
			f[12] == other.f[12] && f[13] == other.f[13] && f[14] == other.f[14] && f[15] == other.f[15]);
}

bool AMatrix::operator != (const AMatrix& other) const
{
	return !(*this == other);
}

AMatrix& AMatrix::operator *= (const float k)
{
	f[0] *= k;
	f[1] *= k;
	f[2] *= k;
	//f[3] *= k;	// should always be 0.0f
	f[4] *= k;
	f[5] *= k;
	f[6] *= k;
	//f[7] *= k;	// 0.0f
	f[8] *= k;
	f[9] *= k;
	f[10] *= k;
	//f[11] *= k;	// 0.0f
	f[12] *= k;
	f[13] *= k;
	f[14] *= k;
	//f[15] = 1.0f;	// should always be 1.0f
	
	return *this;
}

AMatrix& AMatrix::operator *= (const AMatrix& m)
{
	// use temporary floats to hold new matrix values (temp is currently in use)
	a0 = f[0]*m.f[0] + f[4]*m.f[1] + f[8]*m.f[2] + f[12]*m.f[3];
	a1 = f[1]*m.f[0] + f[5]*m.f[1] + f[9]*m.f[2] + f[13]*m.f[3];
	a2 = f[2]*m.f[0] + f[6]*m.f[1] + f[10]*m.f[2] + f[14]*m.f[3];
	//a3 = f[3]*m.f[0] + f[7]*m.f[1] + f[11]*m.f[2] + f[15]*m.f[3];		// 0.0f
	 
	a4 = f[0]*m.f[4] + f[4]*m.f[5] + f[8]*m.f[6] + f[12]*m.f[7];
	a5 = f[1]*m.f[4] + f[5]*m.f[5] + f[9]*m.f[6] + f[13]*m.f[7];
	a6 = f[2]*m.f[4] + f[6]*m.f[5] + f[10]*m.f[6] + f[14]*m.f[7];
	//a7 = f[3]*m.f[4] + f[7]*m.f[5] + f[11]*m.f[6] + f[15]*m.f[7];		// 0.0f
	 
	a8 = f[0]*m.f[8] + f[4]*m.f[9] + f[8]*m.f[10] + f[12]*m.f[11];
	a9 = f[1]*m.f[8] + f[5]*m.f[9] + f[9]*m.f[10] + f[13]*m.f[11];
	a10 = f[2]*m.f[8] + f[6]*m.f[9] + f[10]*m.f[10] + f[14]*m.f[11];
	//a11 = f[3]*m.f[8] + f[7]*m.f[9] + f[11]*m.f[10] + f[15]*m.f[11];	// 0.0f
	 
	a12 = f[0]*m.f[12] + f[4]*m.f[13] + f[8]*m.f[14] + f[12]*m.f[15];
	a13 = f[1]*m.f[12] + f[5]*m.f[13] + f[9]*m.f[14] + f[13]*m.f[15];
	a14 = f[2]*m.f[12] + f[6]*m.f[13] + f[10]*m.f[14] + f[14]*m.f[15];
	//a15 = f[3]*m.f[12] + f[7]*m.f[13] + f[11]*m.f[14] + f[15]*m.f[15];	// 1.0f
	
	// assign new values to this matrix
	f[0]=a0;	f[4]=a4;	f[8]=a8;	f[12]=a12;
	f[1]=a1;	f[5]=a5;	f[9]=a9;	f[13]=a13;
	f[2]=a2;	f[6]=a6;	f[10]=a10;	f[14]=a14;
	f[3]=0.0f;	f[7]=0.0f;	f[11]=0.0f; f[15]=1.0f;
	
	return *this;
}

AVector AMatrix::operator * (const AVector& v) const
{
	return AVector(f[0]*v[0] + f[4]*v[1] + f[8]*v[2] + f[12]*v[3],
				   f[1]*v[0] + f[5]*v[1] + f[9]*v[2] + f[13]*v[3],
				   f[2]*v[0] + f[6]*v[1] + f[10]*v[2] + f[14]*v[3],
				   0.0f);	// since matrix * vector
}

float& AMatrix::operator [] (const int x)		// this makes more sense to me (use like an array of floats)
{
	return f[x];
}

const float& AMatrix::operator [] (const int x) const		// this makes more sense to me (use like an array of floats)
{
	return f[x];
}



/**********************************************
 * FRIEND OPERATOR OVERLOADS
 **********************************************/
AVector operator * (const AVector& v, const AMatrix& m)
{
	return m*v;
}

AVector& operator *= (AVector& v, const AMatrix& m)
{
	m.a0 = m[0]*v[0] + m[4]*v[1] + m[8]*v[2] + m[12]*v[3];
	m.a1 = m[1]*v[0] + m[5]*v[1] + m[9]*v[2] + m[13]*v[3];
	m.a2 = m[2]*v[0] + m[6]*v[1] + m[10]*v[2] + m[14]*v[3];
	
	v[0] = m.a0;
	v[1] = m.a1;
	v[2] = m.a2;
	//v[3] = 0.0f;	// should always be 0.0f since vector
	
	return v;
}

AMatrix operator * (const AMatrix& m, const float k)	// could be faster but don't really use
{
	return AMatrix(m.f[0]*k,	m.f[4]*k,	m.f[8]*k,	m.f[12]*k,
				   m.f[1]*k,	m.f[5]*k,	m.f[9]*k,	m.f[13]*k,
				   m.f[2]*k,	m.f[6]*k,	m.f[10]*k,	m.f[14]*k,
				   0.0f,		0.0f,		0.0f,		1.0f);
}

AMatrix operator * (const float k, const AMatrix& m)
{
	return m*k;
}

AMatrix operator * (const AMatrix& a, const AMatrix& b)
{
	return AMatrix(a.f[0]*b.f[0] + a.f[4]*b.f[1] + a.f[8]*b.f[2] + a.f[12]*b.f[3],
				   a.f[1]*b.f[0] + a.f[5]*b.f[1] + a.f[9]*b.f[2] + a.f[13]*b.f[3],
				   a.f[2]*b.f[0] + a.f[6]*b.f[1] + a.f[10]*b.f[2] + a.f[14]*b.f[3],
				   0.0f,
				   
				   a.f[0]*b.f[4] + a.f[4]*b.f[5] + a.f[8]*b.f[6] + a.f[12]*b.f[7],
				   a.f[1]*b.f[4] + a.f[5]*b.f[5] + a.f[9]*b.f[6] + a.f[13]*b.f[7],
				   a.f[2]*b.f[4] + a.f[6]*b.f[5] + a.f[10]*b.f[6] + a.f[14]*b.f[7],
				   0.0f,
				   
				   a.f[0]*b.f[8] + a.f[4]*b.f[9] + a.f[8]*b.f[10] + a.f[12]*b.f[11],
				   a.f[1]*b.f[8] + a.f[5]*b.f[9] + a.f[9]*b.f[10] + a.f[13]*b.f[11],
				   a.f[2]*b.f[8] + a.f[6]*b.f[9] + a.f[10]*b.f[10] + a.f[14]*b.f[11],
				   0.0f,
				   
				   a.f[0]*b.f[12] + a.f[4]*b.f[13] + a.f[8]*b.f[14] + a.f[12]*b.f[15],
				   a.f[1]*b.f[12] + a.f[5]*b.f[13] + a.f[9]*b.f[14] + a.f[13]*b.f[15],
				   a.f[2]*b.f[12] + a.f[6]*b.f[13] + a.f[10]*b.f[14] + a.f[14]*b.f[15],
				   1.0f);
}

// display matrix as column ordered (OpenGL default)
std::ostream& operator << (std::ostream& os, const AMatrix& m)
{
	os << "{" << m.f[0] << ", " << m.f[4] << ", " << m.f[8] << ", " << m.f[12] << ",\n"
	<< " " << m.f[1] << ", " << m.f[5] << ", " << m.f[9] << ", " << m.f[13] << ",\n"
	<< " " << m.f[2] << ", " << m.f[6] << ", " << m.f[10] << ", " << m.f[14] << ",\n"
	<< " " << m.f[3] << ", " << m.f[7] << ", " << m.f[11] << ", " << m.f[15] << "}";
	
	return os;
}



/**********************************************
 * PUBLIC FUNCTIONS
 **********************************************/
// create a projection to "squish" object into plane. supposedly an easy way of drawing
// shadows onto the ground. must have a plane equation from GetPlaneEquation()
AMatrix MakePlanarShadowMatrix(const AVector& vPlane, const AVector& vLightPos)
{
	// make code easier to read.
	float a = vPlane[0];
	float b = vPlane[1];
	float c = vPlane[2];
	float d = vPlane[3];
	
	float dx = -vLightPos[0];
	float dy = -vLightPos[1];
	float dz = -vLightPos[2];
	
	// now build and return the projection matrix
	return AMatrix(
				   b * dy + c * dz,
				   -a * dy,
				   -a * dz,
				   0.0f,
				   
				   -b * dx,
				   a * dx + c * dz,
				   -b * dz,
				   0.0f,
				   
				   -c * dx,
				   -c * dy,
				   a * dx + b * dy,
				   0.0f,
				   
				   -d * dx,
				   -d * dy,
				   -d * dz,
				   a * dx + b * dy + c * dz);
}



/**********************************************
 * EFFICIENT FUNCTIONS
 **********************************************/

// build the lookup table (call on startup! must call before using AMatrix class!)
void BuildSinCosLut(void)
{
	for ( int angle=0; angle < 360; ++angle )
	{
		sinLut[angle] = std::sin(angle/RAD_TO_DEGREE);
		cosLut[angle] = std::cos(angle/RAD_TO_DEGREE);
	}
}

void Multiply(AVector& dst, const AMatrix& m, const AVector& v)
{
	dst[0] = m[0]*v[0] + m[4]*v[1] + m[8]*v[2] + m[12]*v[3];
	dst[1] = m[1]*v[0] + m[5]*v[1] + m[9]*v[2] + m[13]*v[3];
	dst[2] = m[2]*v[0] + m[6]*v[1] + m[10]*v[2] + m[14]*v[3];
	dst[3] = 0.0f;
}








