//
//  xz_circle.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-03.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef XZ_CIRCLE_H_
#define XZ_CIRCLE_H_

#include <iostream>
#include "a_vector.h"

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	XZCircle: circle object used for circle2circle and circle2square collisions ("a_actor.h")
 *|		in the xz plane with y=0
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class XZCircle
{
private:
	
public:
	float rad;	// public since this class is really just a float and a point
	APoint center;
	
	XZCircle(void)
	{
		rad = 0.0f;
		center.Set(0.0f, 0.0f, 0.0f);
	}
	
	XZCircle(const XZCircle& c)
	{
		rad = c.rad;
		center = c.center;
	}
	
	~XZCircle(void)
	{
		// silent do-nothing destructor
	}
	
	// initialize circle with radius r
	void Init(APoint vLocation, const float r)
	{
		rad = r;
		Update(vLocation);
	}
	
	// set circle's radius
	void Update(APoint vLocation)
	{
		// bounding circles are flattened to the y=0 xz plane
		center.Set(vLocation[0], 0.0f, vLocation[2]);
	}
	
	friend std::ostream& operator << (std::ostream& os, const XZCircle& c)
	{
		os << "\tcircle with radius: " << c.rad << std::endl;
		os << "\tcenter: " << c.center;
		
		return os;
	}
	
};	// end of class XZCircle


#endif	// CZ_CIRCLE_H_




