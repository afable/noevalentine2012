//
//  a_actor.cpp
//  prog0-2_valentines
//
//  Created by afable on 12-04-28.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_actor.h"


/* INIT STATIC VARIABLES */
// temp matrix and vector for fast calculations
AMatrix AActor::m;
AVector AActor::vTemp;

// box1 temp points in world coords
APoint AActor::left;		// leftmost vertex for box1
APoint AActor::right;	// rightmost vertex, etc...
APoint AActor::up;
APoint AActor::down;

// collision test vectors for box1
AVector AActor:: l2d;
AVector AActor:: d2r;
AVector AActor:: r2u;
AVector AActor:: u2l;

// box2 temp points in world coords
APoint AActor::left2;	// leftmost vertex for for box2, etc.
APoint AActor::right2;
APoint AActor::up2;
APoint AActor::down2;

// collision test vectors for box2
AVector AActor:: left2test;
AVector AActor:: down2test;
AVector AActor:: right2test;
AVector AActor:: up2test;

// circle2circle vector
AVector AActor:: center;

// circle2square vectors;
float AActor::dist[4];
AVector AActor::close[4];
AVector AActor::corner2corner;
AVector AActor::corner2center;
APoint AActor::closestPoint;
AVector AActor::closestVector;

// for eye not moving but being pushed against walls
AVector AActor::pusher2pushee;





