//
//  a_vector.h
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_VECTOR_H_
#define A_VECTOR_H_

#include <iostream>

const float SIN45 = 0.7071068f;

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AVector: 4d vector class for all vector transformations.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AVector
{
private:
	float f[4];
	
public:	
	/* CONSTRUCTOR & DESTRUCTOR */
	AVector(void);		// overwrite default constructor
	AVector(const float x, const float y, const float z, const float w=0.0f);
	AVector(const AVector& copy);	// overwrite copy constructor
	~AVector(void);		// overwrite default destructor
	
	/* SETTERS */
	void Set(const float x, const float y, const float z, const float w=0.0f);
	void SetY(const float y);
	void Normalize(void); // makes vector unit length
	void Zero(void); // assigns zeros to all vector values
	
	/* GETTERS */
	float GetMagnitude(void); // returns length of vector
	AVector GetUnitVector(void); // returns a unit vector of this vector. no side effects
	
	/* MEMBER FUNCTIONS */
	bool IsPoint(void) const;
	
	/* OPERATOR OVERLOADS */
	AVector& operator = (const AVector& v);	// overwrite default assignment operator
	AVector operator + (const AVector& v) const;
	AVector operator - (const AVector& v) const;
	AVector operator * (const float s) const;
	
	const AVector& operator += (const AVector& v);
	const AVector& operator -= (const AVector& v);
	const AVector& operator *= (const float s);
	
	bool operator == (const AVector& v);
	bool operator != (const AVector& v);
	
	float& operator [] (const int x);		// this makes more sense to me (use like an array of floats)
	const float& operator [] (const int x) const;		// this makes more sense to me (use like an array of floats)
	
	/* FRIEND OPERATOR OVERLOADS */
	friend AVector operator * (const float s, const AVector& v);
	friend std::ostream& operator << (std::ostream& os, const AVector& v);
	
}; // end of class AVector


// a point is just a vector with w=1 (vector has w=0)
typedef AVector APoint;


/* PUBLIC FUNCTIONS */
AVector CrossProduct(const AVector& lhv, const AVector& rhv); 
AVector FindNormal(const APoint& p1, const APoint& p2, const APoint& p3);
AVector GetPlaneEquation(const APoint& p1, const APoint& p2, const APoint& p3);

/* EFFICIENT FUNCTIONS */
float InvSqrt(float x);	// inverse sqrt used in Quake 3 engine
void Norm(AVector& v);
void GetUnitVector(AVector& dst, AVector& v);
void Add(AVector& dst, const AVector& v1, const AVector& v2);
void Subtract(AVector& dst, const AVector& v1, const AVector& v2);
void Multiply(AVector& dst, const AVector& v1, const float s);
void CrossProduct(AVector& dst, const AVector& lhv, const AVector& rhv); 
float DotProduct(const AVector& lhv, const AVector& rhv);
void FindNormal(AVector& n, const APoint& p1, const APoint& p2, const APoint& p3);
void GetPlaneEquation(AVector& u, const APoint& p1, const APoint& p2, const APoint& p3);
void Project(AVector& dst, const AVector& u, const AVector& v);
float absv(const float f);

#endif  // A_VECTOR_H_





