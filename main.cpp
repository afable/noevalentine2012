//
//  main.cpp
//  prog0-1
//
//  Created by afable on 12-04-25.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


#include <iostream>
#include <cmath>
#ifdef __linux__
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_opengl.h>
	#include <SDL/SDL_mixer.h>
#else
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <SDL/SDL.h>
	#include <SDL/SDL_opengl.h>
	#include <SDL_mixer/SDL_mixer.h>
#endif
#include "a_actor.h"
#include "a_block.h"
#include "a_boat.h"
#include "a_elevator.h"
#include "a_eye.h"
#include "a_matrix.h"
#include "a_monster.h"
#include "a_timer.h"
#include "a_vector.h"
#include "a_water_monster.h"
#include "glm.h"
#include "glm.c"

// file-scope internal linkage unnamed namespace
namespace 
{
	// window properties
	SDL_Surface* window = NULL;
	const int WINDOW_WIDTH= 1280;
	const int WINDOW_HEIGHT = 720;
	const int WINDOW_BPP = 32;
	
	// frame rate properties
	const int FRAMES_PER_SECOND = 60;
	const int TICKS_PER_SECOND = 1000;
	
	// SDL_mixer properties
	const int MIX_DEFAULT_CHUNKSIZE = 4096;
	
	// display list identifiers
	GLuint lEye[2];		// normal=0, hit=1
	const int ID_EYE = 9999;
	GLuint lCompass, lGround, lSphere, lTriangle;
	const int ID_COMPASS = 1;
	const int ID_GROUND = 2;
	const int ID_SPHERE = 3;
	const int ID_TRIANGLE = 4;
	const int ID_SMALL_BLOCK = 22;
	const int ID_BIG_BLOCK = 55;
	const int ID_HUGE_BLOCK = 88;
	int nElevatorIds = 333;
	int nBoatIds = 450;
	int nMonsterIds = 1988;
	const int ID_MONSTER = 1980;
	const int ID_WATER_MONSTER = 1981;
	const int ID_HEART = 102;
	
	// eye we use to view the world
	AEye eEye;
	AEye* const eye = &eEye;
	
	// vector lists to hold other actors (note: uses copy constructor)
	std::vector<AActor*> blocks;
	std::vector<AActor*> elevators;
	std::vector<AActor*> monsters;
	std::vector<AActor*> waterMonsters;
	
	// glm models
	::GLMmodel* duck = glmReadOBJ((char*)"./models/duckmodel.obj");
	::GLMmodel* heart = glmReadOBJ((char*)"./models/heartmodel.obj");
	::GLMmodel* boat = glmReadOBJ((char*)"./models/boatmodel.obj");
	::GLMmodel* person = glmReadOBJ((char*)"./models/personmodel.obj");
	::GLMmodel* devil = glmReadOBJ((char*)"./models/devilmodel.obj");
	
	// main loop variables
	ATimer fps;
	SDL_Event event;
	bool done = false;
	
	// function prototypes
	bool InitSDL(void);
	bool InitOpenGL(void);
	void InitActors(void);
	
	void ProcessEvents(void);
	void ProcessReset(void);
	void ProcessLogic(void);
	void Render(void);
	
	void DrawCompass(GLfloat xr, GLfloat xg, GLfloat xb, GLfloat yr, GLfloat yg, GLfloat yb, GLfloat zr, GLfloat zg, GLfloat zb, GLfloat fAlpha);
	void DrawGround(float fExtent, float fStep, float y);
	void DrawSea(float x, float z, float fStep, float y);
	void DrawSphere(double radius, int latitude, int longitude);
	void DrawTriangle(void);
	void DrawBox(float sideLength, float alpha);
	void DrawXZCollisionSquare(float sideLength, float height, float alpha);
	void DrawCircle(float radius, float y, float alpha);
	void DrawDevil(float r, float g, float b, float a, GLMmodel* devil);
	void DrawPerson(float r, float g, float b, float a, GLMmodel* person);
	void DrawBoat(float r, float g, float b, float a, GLMmodel* boat);
	void DrawHeart(float r, float g, float b, float a, GLMmodel* heart);
	void DrawDuck(float r, float g, float b, float a, GLMmodel* duck);
	
	void Reshape(GLsizei w, GLsizei h); // vertical resizing moves objects in opposite vertical direction
	void Quit(void);
}

int main(int argc, char ** argv)
{	
	// init SDL systems
	if ( !InitSDL() )
		exit(EXIT_FAILURE);
	
	// request window attributes for OpenGL window
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);	// sum of red, green, blue, and alpha component buffer
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);	 // 16-bit depth buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);	// request a double buffered window
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1); // turn on multisample buffers
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);	// number of multisamples
	
	// create SDL window with SDL_OPENGL flag
	if ( !(::window = SDL_SetVideoMode(::WINDOW_WIDTH, ::WINDOW_HEIGHT, ::WINDOW_BPP, SDL_OPENGL)) )
	{
		std::cerr << SDL_GetError() << std::endl;
		exit(EXIT_FAILURE);
	}
	
	// init OpenGL
	if ( !InitOpenGL() )
		exit(EXIT_FAILURE);
	
	// set window caption
	SDL_WM_SetCaption("prog0-2_valentines", NULL);
	
	// build AMatrix sin and cos lookup tables
	BuildSinCosLut();
	
	// initialize actor positions and rotations (access vector list)
	InitActors();
	
	//  main game loop
    while( !::done )
	{
		// fps reset
		::fps.Start();
		
		// process incoming events: handle events (key input) and set object velocities
        ProcessEvents();
		
		// handle logic: move objects, check collisions (and move back if necessary)
		ProcessLogic();
		
		// render screen: draw objects final position in frame to screen
		Render();
		
		// fps wait
		if ( fps.GetTicks() < TICKS_PER_SECOND/FRAMES_PER_SECOND )
			SDL_Delay(TICKS_PER_SECOND/FRAMES_PER_SECOND - fps.GetTicks());	// ~60 fps
    }
	
	// quit systems
	Quit();
	
	return EXIT_SUCCESS;
}

namespace 
{
	bool InitSDL(void)
	{
		// init SDL
		if ( SDL_Init(SDL_INIT_VIDEO) || SDL_Init(SDL_INIT_EVENTTHREAD) )
		{
			std::cerr << SDL_GetError() << std::endl;
			return false;
		}
		
//		// init SDL_mixer
//		if ( Mix_Init(MIX_INIT_MP3) != MIX_INIT_MP3 || Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, ::MIX_DEFAULT_CHUNKSIZE) )
//		{
//			std::cerr << SDL_GetError() << std::endl;
//			return false;
//		}
		
		// if SDL initialized okay
		return true;
	}
}

namespace 
{
	bool InitOpenGL(void)
	{
		glClearColor(0.8f, 0.7f, 0.6f, 1.0f);
		glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
		
		// enable depth test
		glEnable(GL_DEPTH_TEST);
		
		// enable smooth shading progression between colored vertices (default)
		glShadeModel(GL_SMOOTH);
		
		// enable ccw polygon winding (default)
		glFrontFace(GL_CCW);
		
		// draw back face of polygons as line (cull for performance)
		glPolygonMode(GL_BACK, GL_LINE);
		
		// cull back face of polygons (default)
		//		glCullFace(GL_BACK);
		//		glEnable(GL_CULL_FACE);
		
		// enable blending of color alpha components
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		
		// enable multisampling to get rid of jaggies (can you tell?)
		glEnable(GL_MULTISAMPLE);
		
		// another antialiasing method (may work better for points than multisampling)
		//		glEnable(GL_POINT_SMOOTH);
		//		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		//		glEnable(GL_LINE_SMOOTH);
		//		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		//		glEnable(GL_POLYGON_SMOOTH);
		//		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		
		// turn on fog
		//		float fFogColor[4] = {1.0f, 1.0f, 1.0f, 1.0f};
		//		glEnable(GL_FOG);			// turn fog on
		//		glFogfv(GL_FOG_COLOR, fFogColor);	// set fog color
		//		glFogf(GL_FOG_START, 0.0f);		// how far away does fog start
		//		glFogf(GL_FOG_END, 5.0f);		// how far away does fog stop (fully fogged)
		//		glFogi(GL_FOG_MODE, GL_LINEAR);	// which fog equation to use
		
		
		// set standard output to display 2 decimals for vector and matrix debugging
		std::cerr.std::ios_base::setf(std::ios::fixed);
		std::cerr.std::ios_base::precision(2);
		
		// get a random seed for srand
		srand(time(NULL));
		
		Reshape(::WINDOW_WIDTH, ::WINDOW_HEIGHT);
		
		if ( glGetError() != GL_NO_ERROR )
		{
			return false;
		}

		// if OpenGL initialized okay
		return true;
	}
}

namespace 
{
	void InitActors(void)
	{
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *|	Initialize Actor Objects
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		
		/*********************************************
		 * EYE
		 *********************************************/
		// initialize eye starting location, direction (-z into screen OpenGL default) and collision data.
		::eye->Init(::ID_EYE, ::EYE_X_LOC, ::EYE_Y_LOC, ::EYE_Z_LOC, ::EYE_HEIGHT, ::EYE_RADIUS, ::EYE_TRANS_SPEED, ::EYE_ROT_SPEED, ::EYE_X_ROT, ::EYE_Y_ROT);
		
		/*********************************************
		 * NON-MOVING BLOCKS
		 *********************************************/
		// populate and initialize non-moving blocks
		ABlock* island;
		const int NUM_BLOCKS = 92;
		for ( int i = 0; i < NUM_BLOCKS; ++i )
			::blocks.push_back(island);
		for ( int i = 0; i < NUM_BLOCKS; ++i )
			::blocks[i] = new ABlock;
		
		// starting island
		::blocks[0]->Init(::ID_SMALL_BLOCK, 0.0f, 0.0f, 0.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[1]->Init(::ID_SMALL_BLOCK, -1.0f, -0.5f, 0.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[2]->Init(::ID_SMALL_BLOCK, 0.0f, -0.5f, -1.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[3]->Init(::ID_SMALL_BLOCK, 1.0f, -0.5f, 0.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[4]->Init(::ID_SMALL_BLOCK, 0.0f, -0.5f, 1.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[5]->Init(::ID_SMALL_BLOCK, 4.0f, -0.5f, -1.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[6]->Init(::ID_SMALL_BLOCK, 0.0f, -0.5f, -2.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// first island north
		::blocks[7]->Init(::ID_BIG_BLOCK, 0.0f, -0.5f, -60.0f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[8]->Init(::ID_SMALL_BLOCK, -2.5f, -0.5f, -60.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[9]->Init(::ID_SMALL_BLOCK, 0.0f, -0.5f, -57.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[10]->Init(::ID_SMALL_BLOCK, 2.5f, -0.5f, -59.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[11]->Init(::ID_SMALL_BLOCK, 2.5f, -0.5f, -60.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[12]->Init(::ID_SMALL_BLOCK, 0.0f, 0.0f, -62.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[13]->Init(::ID_SMALL_BLOCK, 0.0f, 1.0f, -63.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// block after elevator and stepping block and fork blocks
		::blocks[14]->Init(::ID_SMALL_BLOCK, 0.0f, 40.0f, -66.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[15]->Init(::ID_SMALL_BLOCK, 0.0f, 38.9f, -67.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[16]->Init(::ID_SMALL_BLOCK, -1.5f, 40.0f, -64.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[17]->Init(::ID_SMALL_BLOCK, 2.5f, 40.0f, -63.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// left hard to jump route to main monster battle
		::blocks[18]->Init(::ID_BIG_BLOCK, 5.0f, 40.0f, -58.0f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[19]->Init(::ID_BIG_BLOCK, 9.0f, 40.0f, -58.0f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[20]->Init(::ID_BIG_BLOCK, 12.0f, 40.0f, -51.5f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[21]->Init(::ID_SMALL_BLOCK, 15.0f, 40.0f, -47.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// right easier route to main monster battle
		::blocks[22]->Init(::ID_SMALL_BLOCK, -4.0f, 40.0f, -62.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[23]->Init(::ID_SMALL_BLOCK, -2.5f, 40.0f, -59.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[24]->Init(::ID_SMALL_BLOCK, -3.5f, 40.0f, -55.8f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[25]->Init(::ID_SMALL_BLOCK, -3.5f, 40.0f, -54.8f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[26]->Init(::ID_SMALL_BLOCK, -3.5f, 40.0f, -53.8f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[27]->Init(::ID_SMALL_BLOCK, -3.5f, 40.0f, -52.8f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[28]->Init(::ID_BIG_BLOCK, -1.0f, 40.0f, -48.0f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[29]->Init(::ID_SMALL_BLOCK, 3.7f, 40.0f, -45.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[30]->Init(::ID_SMALL_BLOCK, 7.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[31]->Init(::ID_SMALL_BLOCK, 10.3f, 40.0f, -43.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// 1st drop
		::blocks[32]->Init(::ID_SMALL_BLOCK, 13.0f, 40.0f, -43.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[33]->Init(::ID_SMALL_BLOCK, 13.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[34]->Init(::ID_SMALL_BLOCK, 14.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[35]->Init(::ID_SMALL_BLOCK, 15.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[36]->Init(::ID_SMALL_BLOCK, 16.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[37]->Init(::ID_SMALL_BLOCK, 17.0f, 40.0f, -44.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[38]->Init(::ID_SMALL_BLOCK, 17.0f, 40.0f, -43.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[39]->Init(::ID_SMALL_BLOCK, 17.0f, 40.0f, -42.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[40]->Init(::ID_SMALL_BLOCK, 17.0f, 40.0f, -41.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[41]->Init(::ID_SMALL_BLOCK, 17.0f, 40.0f, -40.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[42]->Init(::ID_SMALL_BLOCK, 16.0f, 40.0f, -40.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[43]->Init(::ID_SMALL_BLOCK, 15.0f, 40.0f, -40.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[44]->Init(::ID_SMALL_BLOCK, 14.0f, 40.0f, -40.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[45]->Init(::ID_SMALL_BLOCK, 13.0f, 40.0f, -40.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[46]->Init(::ID_SMALL_BLOCK, 13.0f, 40.0f, -41.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[47]->Init(::ID_SMALL_BLOCK, 13.0f, 40.0f, -42.0f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// 2nd drop
		::blocks[48]->Init(::ID_BIG_BLOCK, 15.0f, 30.0f, -32.0f, ::BIG_BLOCK_HEIGHT, ::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[49]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -32.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[50]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -33.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[51]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -34.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[52]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[53]->Init(::ID_SMALL_BLOCK, 17.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[54]->Init(::ID_SMALL_BLOCK, 16.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[55]->Init(::ID_SMALL_BLOCK, 15.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[56]->Init(::ID_SMALL_BLOCK, 14.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[57]->Init(::ID_SMALL_BLOCK, 13.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[58]->Init(::ID_SMALL_BLOCK, 12.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[59]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -35.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[60]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -34.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[61]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -33.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[62]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -32.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[63]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -31.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[64]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -30.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[65]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -29.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[66]->Init(::ID_SMALL_BLOCK, 11.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[67]->Init(::ID_SMALL_BLOCK, 12.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[68]->Init(::ID_SMALL_BLOCK, 13.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[69]->Init(::ID_SMALL_BLOCK, 14.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[70]->Init(::ID_SMALL_BLOCK, 15.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[71]->Init(::ID_SMALL_BLOCK, 16.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[72]->Init(::ID_SMALL_BLOCK, 17.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[73]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -28.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[74]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -29.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[75]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -30.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[76]->Init(::ID_SMALL_BLOCK, 18.5f, 30.0f, -31.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// final landing!!!
		::blocks[77]->Init(::ID_HUGE_BLOCK, 15.0f, 20.0f, -20.0f, ::HUGE_BLOCK_HEIGHT, ::HUGE_BLOCK_SIDE_LENGTH, ::HUGE_BLOCK_RADIUS, 0.0f, 0.0f);
		
		// random blocks to make water monsters jump throughout the sea belows ( monster2block edge collisions flattened to y=0 )
		::blocks[78]->Init(::ID_SMALL_BLOCK, 0.0f, 15.5f, -11.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[79]->Init(::ID_SMALL_BLOCK, 0.0f, 15.5f, -5.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[80]->Init(::ID_SMALL_BLOCK, 3.0f, 15.5f, -6.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[81]->Init(::ID_SMALL_BLOCK, 4.0f, 15.5f, -7.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[82]->Init(::ID_SMALL_BLOCK, 8.5f, 15.5f, -9.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[83]->Init(::ID_SMALL_BLOCK, -2.0f, 15.5f, -4.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[84]->Init(::ID_SMALL_BLOCK, -8.0f, 15.5f, -11.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[85]->Init(::ID_SMALL_BLOCK, 0.0f, 18.0f, -11.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[86]->Init(::ID_SMALL_BLOCK, 5.0f, 18.0f, -14.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[87]->Init(::ID_SMALL_BLOCK, -3.5f, 18.0f, -12.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[88]->Init(::ID_SMALL_BLOCK, 3.0f, 18.0f, -21.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[89]->Init(::ID_SMALL_BLOCK, 0.0f, 18.0f, -21.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[90]->Init(::ID_SMALL_BLOCK, -5.0f, 35.0f, -30.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		::blocks[91]->Init(::ID_SMALL_BLOCK, -3.0f, 35.0f, -34.5f, ::SMALL_BLOCK_HEIGHT, ::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_RADIUS, 0.0f, 0.0f);
		
		
		/*********************************************
		 * MOVING BLOCKS (BOATS AND ELEVATORS)
		 *********************************************/
		// populate and initialize boats
		static ABoat boBoats[1];
		boBoats[0].Init(++::nBoatIds, 0.0f, 0.0f, -4.0f, ::BOAT_HEIGHT, ::BOAT_SIDE_LENGTH, ::BOAT_RADIUS, ::BOAT_TRANS_SPEED, ::BOAT_ROT_SPEED, 0.0f, 0.0f);
		::elevators.push_back(&boBoats[0]);		// elevators[0] is a boat
		
		// populate and initialize elevators
		static AElevator elElevators[1];
		elElevators[0].Init(++::nElevatorIds, 0.0f, 1.5f, -66.0f, ::ELEVATOR_HEIGHT, ::ELEVATOR_SIDE_LENGTH, ::ELEVATOR_RADIUS, ::ELEVATOR_TRANS_SPEED, ::ELEVATOR_ROT_SPEED, 0.0f, 180.0f);
		::elevators.push_back(&elElevators[0]);

		/*********************************************
		 * MONSTERS
		 *********************************************/
		// populate and initialize monsters locations, directions, and collision data
		AMonster* moMonsters;
		const int NUM_MONSTER = 29;
		for ( int i = 0; i < NUM_MONSTER; ++i)
			::monsters.push_back(moMonsters);
		for ( int i = 0; i < NUM_MONSTER; ++i)
			monsters[i] = new AMonster;
		
		// monsters in final landing!!! (normals)
		monsters[0]->Init(++::nMonsterIds, 13.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[1]->Init(++::nMonsterIds, 13.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[2]->Init(++::nMonsterIds, 12.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[3]->Init(++::nMonsterIds, 12.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[4]->Init(++::nMonsterIds, 14.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[5]->Init(++::nMonsterIds, 14.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[6]->Init(++::nMonsterIds, 15.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[7]->Init(++::nMonsterIds, 15.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[8]->Init(++::nMonsterIds, 16.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[9]->Init(++::nMonsterIds, 16.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[10]->Init(++::nMonsterIds, 17.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[11]->Init(++::nMonsterIds, 17.5f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[12]->Init(++::nMonsterIds, 18.0f, 21.0f, -22.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[13]->Init(++::nMonsterIds, 14.5f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[14]->Init(++::nMonsterIds, 15.0f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[15]->Init(++::nMonsterIds, 15.5f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		// monsters in final landing!!! (speedy devils)
		monsters[16]->Init(++::nMonsterIds, 14.5f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_SPEEDY_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 180.0f);
		monsters[17]->Init(++::nMonsterIds, 15.0f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_SPEEDY_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 180.0f);
		monsters[18]->Init(++::nMonsterIds, 15.5f, 21.0f, -21.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_SPEEDY_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 180.0f);
		
		// monsters in 2nd drop (normal)
		monsters[19]->Init(++::nMonsterIds, 13.5f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[20]->Init(++::nMonsterIds, 14.0f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[21]->Init(++::nMonsterIds, 14.5f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[22]->Init(++::nMonsterIds, 15.0f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[23]->Init(++::nMonsterIds, 15.5f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[24]->Init(++::nMonsterIds, 16.0f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		monsters[25]->Init(++::nMonsterIds, 16.5f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		// monsters in 2nd drop (speedy)
		monsters[26]->Init(++::nMonsterIds, 15.0f, 31.0f, -32.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_SPEEDY_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 180.0f);		

		// right easier route to main monster battle
		monsters[27]->Init(++::nMonsterIds, -1.0f, 41.0f, -48.0f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		
		// left hard to jump route to main monster battle		
		monsters[28]->Init(++::nMonsterIds, 12.0f, 41.0f, -51.5f, ::MONSTER_HEIGHT, ::MONSTER_RADIUS, ::MONSTER_TRANS_SPEED, ::MONSTER_ROT_SPEED, 0.0f, 0.0f);
		
		/*********************************************
		 * WATER MONSTERS
		 *********************************************/
		// populate and initialize monsters locations, directions, and collision data
		AWaterMonster* waMonsters;
		const int NUM_WATER_MONSTER = 51;
		for ( int i = 0; i < NUM_WATER_MONSTER; ++i)
			::waterMonsters.push_back(waMonsters);
		for ( int i = 0; i < NUM_WATER_MONSTER; ++i)
			waterMonsters[i] = new AWaterMonster;
		
		// water monster that comes towards you at beginning
		waterMonsters[0]->Init(++::nMonsterIds, 0.0f, 1.0f, -10.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 180.0f);
		
		// water monsters that start from your left
		waterMonsters[1]->Init(++::nMonsterIds, -10.0f, 1.0f, -10.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[2]->Init(++::nMonsterIds, -10.0f, 1.0f, -13.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[3]->Init(++::nMonsterIds, -10.0f, 1.0f, -15.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[4]->Init(++::nMonsterIds, -5.0f, 1.0f, -17.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[5]->Init(++::nMonsterIds, -1.0f, 1.0f, -19.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[6]->Init(++::nMonsterIds, 0.0f, 1.0f, -21.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[7]->Init(++::nMonsterIds, -7.0f, 1.0f, -23.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[8]->Init(++::nMonsterIds, -7.0f, 1.0f, -25.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[9]->Init(++::nMonsterIds, -10.0f, 1.0f, -27.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[10]->Init(++::nMonsterIds, -1.0f, 1.0f, -29.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[11]->Init(++::nMonsterIds, -2.0f, 1.0f, -31.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[12]->Init(++::nMonsterIds, -4.0f, 1.0f, -33.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[13]->Init(++::nMonsterIds, -8.0f, 1.0f, -35.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[14]->Init(++::nMonsterIds, 0.0f, 1.0f, -37.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[15]->Init(++::nMonsterIds, -10.0f, 1.0f, -39.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[16]->Init(++::nMonsterIds, -10.0f, 1.0f, -41.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[17]->Init(++::nMonsterIds, -10.0f, 1.0f, -43.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[18]->Init(++::nMonsterIds, -5.0f, 1.0f, -45.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[19]->Init(++::nMonsterIds, -5.0f, 1.0f, -47.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[20]->Init(++::nMonsterIds, -5.0f, 1.0f, -49.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[21]->Init(++::nMonsterIds, -3.0f, 1.0f, -51.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[22]->Init(++::nMonsterIds, -7.0f, 1.0f, -53.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[23]->Init(++::nMonsterIds, -8.0f, 1.0f, -55.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[24]->Init(++::nMonsterIds, -2.0f, 1.0f, -57.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		waterMonsters[25]->Init(++::nMonsterIds, -1.0f, 1.0f, -59.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 225.0f);
		
		// water monsters that start from your right
		waterMonsters[26]->Init(++::nMonsterIds, 10.0f, 1.0f, -10.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[27]->Init(++::nMonsterIds, 10.0f, 1.0f, -13.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[28]->Init(++::nMonsterIds, 10.0f, 1.0f, -15.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[29]->Init(++::nMonsterIds, 5.0f, 1.0f, -17.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[30]->Init(++::nMonsterIds, 1.0f, 1.0f, -19.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[31]->Init(++::nMonsterIds, 0.0f, 1.0f, -21.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[32]->Init(++::nMonsterIds, 7.0f, 1.0f, -23.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[33]->Init(++::nMonsterIds, 7.0f, 1.0f, -25.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[34]->Init(++::nMonsterIds, 10.0f, 1.0f, -27.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[35]->Init(++::nMonsterIds, 1.0f, 1.0f, -29.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[36]->Init(++::nMonsterIds, 2.0f, 1.0f, -31.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[37]->Init(++::nMonsterIds, 4.0f, 1.0f, -33.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[38]->Init(++::nMonsterIds, 8.0f, 1.0f, -35.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[39]->Init(++::nMonsterIds, 0.0f, 1.0f, -37.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[40]->Init(++::nMonsterIds, 10.0f, 1.0f, -39.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[41]->Init(++::nMonsterIds, 10.0f, 1.0f, -41.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[42]->Init(++::nMonsterIds, 10.0f, 1.0f, -43.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[43]->Init(++::nMonsterIds, 5.0f, 1.0f, -45.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[44]->Init(++::nMonsterIds, 5.0f, 1.0f, -47.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[45]->Init(++::nMonsterIds, 5.0f, 1.0f, -49.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[46]->Init(++::nMonsterIds, 3.0f, 1.0f, -51.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[47]->Init(++::nMonsterIds, 7.0f, 1.0f, -53.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[48]->Init(++::nMonsterIds, 8.0f, 1.0f, -55.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[49]->Init(++::nMonsterIds, 2.0f, 1.0f, -57.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		waterMonsters[50]->Init(++::nMonsterIds, 1.0f, 1.0f, -59.0f, ::WATER_MONSTER_HEIGHT, ::WATER_MONSTER_RADIUS, ::WATER_MONSTER_TRANS_SPEED, ::WATER_MONSTER_ROT_SPEED, 0.0f, 135.0f);
		

		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *|	Initialize Actor Display Lists
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		
		/*********************************************
		 * INTIIALZE ACTOR DISPLAY LISTS
		 *********************************************/
		
		// get display list names for standard drawings
		::lCompass = ::ID_COMPASS;
		::lGround = ::ID_GROUND;
		::lSphere = ::ID_SPHERE;
		::lTriangle = ::ID_TRIANGLE;
		// prebuild display lists
		glNewList(::lCompass, GL_COMPILE);
		DrawCompass(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.5f);
		DrawBox(1.0f, 0.3f);	// draw bounding box for square collision detection
		DrawCircle(0.5f, -0.5f, 0.3f);	// draw bounding circle for circle collision detection
		glEndList();
		
		glNewList(::lGround, GL_COMPILE);
		glColor4f(0.5f, 0.5f, 1.0f, 0.6f);
		DrawSea(10.0f, 40.0f, 0.2f, WATER_LEVEL);
		glEndList();
		
		glNewList(::lSphere, GL_COMPILE);
		glColor4f(0.3f, 0.3f, 1.0f, 0.6f);
		DrawSphere(0.5f, 3, 10);
		glEndList();
		
		glNewList(::lTriangle, GL_COMPILE);
		DrawTriangle();
		glEndList();
		
		
		// get display list name for eye
//		::lEye[0] = ::ID_EYE;
//		glNewList(::lEye[0], GL_COMPILE);
//		DrawCompass(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.5f);
//		DrawCircle(::EYE_RADIUS, ::EYE_HEIGHT/2.0f, 0.2f);
//		DrawCircle(::EYE_RADIUS, -::EYE_HEIGHT/2.0f, 0.2f);
//		glEndList();
		
		// get display list name for eye (@siefkenj person obj model)
		::lEye[0] = ::ID_EYE;
		glNewList(::lEye[0], GL_COMPILE);
		DrawPerson(1.0f, 0.71f, 0.75f, 0.6f, ::person);
		glEndList();
		
		// get hit eye display list
//		::lEye[1] = ::ID_EYE+1;
//		glNewList(::lEye[1], GL_COMPILE);
//		DrawCompass(1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.1f);
//		DrawCircle(::EYE_RADIUS, ::EYE_HEIGHT/2.0f, 0.1f);
//		DrawCircle(::EYE_RADIUS, -::EYE_HEIGHT/2.0f, 0.1f);
//		glEndList();
		
		// get hit eye display list (@siefkenj person obj model)
		::lEye[1] = ::ID_EYE+1;
		glNewList(::lEye[1], GL_COMPILE);
		DrawPerson(1.0f, 0.31f, 0.35f, 0.33f, ::person);
		glEndList();
		
		// get display list name for small blocks
		glNewList(::ID_SMALL_BLOCK, GL_COMPILE);
		DrawCompass(0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.6f);
		DrawXZCollisionSquare(::SMALL_BLOCK_SIDE_LENGTH, ::SMALL_BLOCK_HEIGHT, 0.6f);	// draw bounding box for square collision detection
		DrawCircle(::SMALL_BLOCK_RADIUS, -0.5f, 0.1f);	// draw bounding circle for circle collision detection
		glColor4f(0.3f, 1.0f, 0.3f, 0.8f);
		DrawGround(::SMALL_BLOCK_SIDE_LENGTH/2.0f, 0.1f, ::SMALL_BLOCK_HEIGHT/2.0f);	// draw island grass
		glEndList();
		
		// get display list name for big blocks
		glNewList(::ID_BIG_BLOCK, GL_COMPILE);
		DrawCompass(0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.6f);
		DrawXZCollisionSquare(::BIG_BLOCK_SIDE_LENGTH, ::BIG_BLOCK_HEIGHT, 0.6f);	// draw bounding box for square collision detection
		DrawCircle(::BIG_BLOCK_RADIUS, -0.5f, 0.1f);	// draw bounding circle for circle collision detection
		glColor4f(0.3f, 1.0f, 0.3f, 0.8f);
		DrawGround(::BIG_BLOCK_SIDE_LENGTH/2.0f, 0.1f, ::BIG_BLOCK_HEIGHT/2.0f);	// draw island grass
		glEndList();
		
		// get display list name for huge blocks
		glNewList(::ID_HUGE_BLOCK, GL_COMPILE);
		DrawCompass(0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.6f);
		DrawXZCollisionSquare(::HUGE_BLOCK_SIDE_LENGTH, ::HUGE_BLOCK_HEIGHT, 0.6f);	// draw bounding box for square collision detection
		DrawCircle(::HUGE_BLOCK_RADIUS, -0.5f, 0.1f);	// draw bounding circle for circle collision detection
		glColor4f(0.3f, 1.0f, 0.3f, 0.8f);
		DrawGround(::HUGE_BLOCK_SIDE_LENGTH/2.0f, 0.1f, ::HUGE_BLOCK_HEIGHT/2.0f);	// draw island grass
		glEndList();
		
		
		// get display list name for boats
//		glNewList(elevators[0]->nId, GL_COMPILE);
//		DrawCompass(0.5f, 0.25f, 0.1f, 0.5f, 0.25f, 0.1f, 1.0f, 1.0f, 1.0f, 0.4f);
//		DrawXZCollisionSquare(::BOAT_SIDE_LENGTH, ::BOAT_HEIGHT, 0.6f);	// draw bounding box for square collision detection
//		DrawCircle(::BOAT_RADIUS, -0.5f, 0.1f);	// draw bounding circle for circle collision detection
//		glColor4f(0.5f, 0.25f, 0.1f, 0.8f);
//		DrawGround(::BOAT_SIDE_LENGTH/2.0f, 0.1f, ::BOAT_HEIGHT/2.0f);	// draw elevator wood
//		glEndList();
		
		// get display list name for boats (siefkenj boat obj model)
		glNewList(elevators[0]->nId, GL_COMPILE);
		DrawBoat(0.42f, 0.22f, 0.12f, 0.4f, ::boat);
		glEndList();
		
		
		// get display list name for elevators
		glNewList(::elevators[1]->nId, GL_COMPILE);
		DrawCompass(0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 1.0f, 1.0f, 1.0f, 0.3f);
		DrawXZCollisionSquare(::ELEVATOR_SIDE_LENGTH, ::ELEVATOR_HEIGHT, 0.6f);	// draw bounding box for square collision detection
		DrawCircle(::ELEVATOR_RADIUS, -0.5f, 0.1f);	// draw bounding circle for circle collision detection
		glColor4f(0.3f, 0.3f, 0.3f, 0.8f);
		DrawGround(::ELEVATOR_SIDE_LENGTH/2.0f, 0.1f, ::ELEVATOR_HEIGHT/2.0f);	// draw elevator wood
		glEndList();
		
		
		// get display list name for monsters
//		glNewList(::ID_MONSTER, GL_COMPILE);
//		DrawCompass(0.5f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.5f);
//		DrawCircle(::MONSTER_RADIUS, ::MONSTER_HEIGHT/2.0f, 0.3f);
//		DrawCircle(::MONSTER_RADIUS, -::MONSTER_HEIGHT/2.0f, 0.3f);
//		glEndList();
		
		// get display list name for monsters (@siefkenj devil obj model)
		glNewList(::ID_MONSTER, GL_COMPILE);
		DrawDevil(0.7f, 0.0f, 0.0f, 0.6f, ::devil);
		glEndList();
		
		// get display list name for water monsters
//		glNewList(::ID_WATER_MONSTER, GL_COMPILE);
//		DrawCompass(0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.5f, 1.0f, 1.0f, 1.0f, 0.5f);
//		DrawCircle(::WATER_MONSTER_RADIUS, ::WATER_MONSTER_HEIGHT/2.0f, 0.3f);
//		DrawCircle(::WATER_MONSTER_RADIUS, -::WATER_MONSTER_HEIGHT/2.0f, 0.3f);
//		glEndList();
		
		// get display list name for ducks	(siefkenj duck obj model)
		glNewList(::ID_WATER_MONSTER, GL_COMPILE);
		DrawDuck(1.0f, 0.84f, 0.0f, 0.4f, ::duck);
		glEndList();
		
		// get display list for monster heart form
//		glNewList(::ID_HEART, GL_COMPILE);
//		DrawCompass(0.5f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.15f);
//		DrawCircle(::MONSTER_RADIUS, ::MONSTER_HEIGHT/2.0f, 0.1f);
//		DrawCircle(::MONSTER_RADIUS, -::MONSTER_HEIGHT/2.0f, 0.1f);
//		glEndList();
		
		// get display list for monster heart form (siefkenj heart obj model)
		glNewList(::ID_HEART, GL_COMPILE);
		DrawHeart(0.8f, 0.16f, 0.36f, 0.4f, ::heart);
		glEndList();
	}
}

namespace 
{
	void ProcessEvents(void)
	{
		// grab all events off event queue
		while( SDL_PollEvent(&::event) )
		{	
			// process window events
			switch (::event.type)
			{
				case SDL_VIDEORESIZE:
					Reshape(::event.resize.w, ::event.resize.h);
					break;
					
				case SDL_KEYDOWN: switch (::event.key.keysym.sym)
				{
					case SDLK_q: ::done = true;
						break;
						
					default:
						break;
				}
					break;
					
				case SDL_KEYUP: switch (::event.key.keysym.sym)
				{
					default:
						break;
				}
					break;
					
				case SDL_MOUSEMOTION:
					if ( ::event.motion.state == 1 || ::event.motion.state == 4 ) // if left or right mouse button clicked
					{
						
					}
					break;
					
				case SDL_MOUSEBUTTONDOWN: // do something if mouse button pressed?
					break;
					
				case SDL_QUIT:
					exit(EXIT_SUCCESS);
					
				default:
					break;
			}
			
			// process non-moving blocks events
			for ( unsigned int i = 0; i < ::blocks.size(); ++i)
				::blocks[i]->ProcessEvents(event);
			
			// process moving blocks events
			for ( unsigned int i = 0; i < ::elevators.size(); ++i)
				::elevators[i]->ProcessEvents(event);	// boats=0, elevators rest
			
			// process eye events
			::eye->ProcessEvents(event);
			
			// process monsters events
			for ( unsigned int i = 0; i < ::monsters.size(); ++i)
				::monsters[i]->ProcessEvents(event);
			
			// process water monsters events
			for ( unsigned int i = 0; i < ::waterMonsters.size(); ++i)
				::waterMonsters[i]->ProcessEvents(event);
		}
	}
}

namespace 
{
	void ProcessReset(void)
	{
		if ( ::eye->bResetEye )
		{
			// reset boat and elevator with eye
			for ( unsigned int i = 0; i < ::elevators.size(); ++i )
				::elevators[i]->Reset();
			
			::eye->bResetEye = false;	// set eye reset to false
		}
	}
}

namespace 
{
	void ProcessLogic(void)
	{
		// if eye is reset, reset boat and elevator as well
		ProcessReset();
		
		// process non-moving blocks logic
		for ( unsigned int i = 0; i < ::blocks.size(); ++i)
			::blocks[i]->ProcessLogic(::eye, ::blocks, ::elevators, ::monsters);	// meet derived class paramter list
		
		// process moving blocks logic (boats/elevators moves eye and monsters, so boats/elevator takes priority)
		for ( unsigned int i = 0; i < ::elevators.size(); ++i)
			::elevators[i]->ProcessLogic(::eye, ::blocks, ::elevators, ::monsters);	// boats=0, elevators rest
		
		// process eye logic
		::eye->ProcessLogic(::blocks, ::elevators, ::monsters, ::waterMonsters);
		
		// process monsters logic
		for ( unsigned int i = 0; i < ::monsters.size(); ++i)
			::monsters[i]->ProcessLogic(::blocks, ::elevators);	// monsters type AActor
		
		// process water monsters logic
		for ( unsigned int i = 0; i < ::waterMonsters.size(); ++i)
			::waterMonsters[i]->ProcessLogic(::blocks, ::elevators);	// monsters type AActor
	}
}

namespace 
{
	void Render(void)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		
		/**********************************************
		 * DRAW OBJECTS IN FRONT OF EYE: Perform initial coordinate transformations to draw
		 * objects-in-front-of-eye in a translated and rotated perspective from original
		 * eye position (eye of camera is always in the same position, objects are just
		 * distorted around this eye to simulate camera movement). Do this for world crawling.
		 * Drawn objects will always appear in front of eye->
		 *
		 * Note: OpenGL coords follow right hand rule. +x right, +y up, +z out of screen.
		 **********************************************/
		// translate forward in -z and start drawing stuff and rotate eye around drawer at (0, 0, -2)
		glTranslatef(0.0f, 0.0f, -2.0f);
		
		// rotate and translate world (for eye) and render images to screen
		if ( ::eye->bHit )
			::eye->Display(::lEye[1]);
		else
			::eye->Display(::lEye[0]);
		
		
		
		
		
		

		

		
		glPushMatrix();
		{
			glTranslatef(0.0f, 0.5045f, -60.0f);
			
//			glCallList(::ID_MONSTER);
		}
		glPopMatrix();


		
		
		
		
		
		
		
		
		
		/**********************************************
		 * DRAW NON-MOVING OBJECTS: Go further into the world and draw non-moving objects.
		 * These objects will be drawn with respect to the initial rotation and transformation
		 * coordinates that were used to setup the objects-in-front-of-camera. Non-moving
		 * objects include ground and environment (trees, etc.) and will not appear "attached"
		 * to the camera's perceived movement.
		 **********************************************/
		
		// move drawer in -z and draw sea
		glPushMatrix();
		{
			glTranslatef(0.0f, 0.0f, -30.0f);
			glCallList(::lGround);
		}
		glPopMatrix();
		
		// draw non-moving blocks
		for ( unsigned int i = 0; i < blocks.size(); ++i)
			::blocks[i]->Display(::blocks[i]->nId);
		
		/**********************************************
		 * DRAW MOVING OBJECTS: Reset coordinate system to objects-in-front-of-camera and go
		 * into world and draw and transform all moving objects (actors).
		 **********************************************/
		
		// draw moving blocks
		for ( unsigned int i = 0; i < ::elevators.size(); ++i)
			::elevators[i]->Display(::elevators[i]->nId);
		
		
		// draw monsters
		for ( unsigned int i = 0; i < ::monsters.size(); ++i)
		{
			if ( ::monsters[i]->bHeart )	// if monster turned into a heart, display heart
			{
				::monsters[i]->Display(::ID_HEART);	// if heart has been taken, delete monster
				if ( ::monsters[i]->bDelete )
				{
					delete ::monsters[i];
					monsters.erase(monsters.begin()+i);	// erase deleted monster pointer from vector
				}
			}
			else	// otherwise, display monster
			{
				::monsters[i]->Display(::ID_MONSTER);
			}
		}
		
		// draw water monsters
		for ( unsigned int i = 0; i < ::waterMonsters.size(); ++i)
		{
			if ( ::waterMonsters[i]->bHeart )	// if water monster turned into a heart, display heart
			{
				::waterMonsters[i]->Display(::ID_HEART);	// if heart has been taken, delete water monster
				if ( ::waterMonsters[i]->bDelete )
				{
					delete ::waterMonsters[i];
					waterMonsters.erase(waterMonsters.begin()+i);	// erase deleted waterMonster pointer from vector
				}
			}
			else	// otherwise, display water monster
			{
				::waterMonsters[i]->Display(::ID_WATER_MONSTER);
			}
		}
		
		
		

//		std::cerr << (*::eye).fRotX << ", " << (*::eye).fRotY << std::endl;
//		std::cerr << *::blocks[7] << std::endl;
//		std::cerr << *::elevators[0] << std::endl;
//		std::cerr << *::monsters[0] << std::endl;
//		std::cerr << (*::waterMonsters[0]).vLocation << std::endl;
		
		
		
		
		
		
		// last item drawn does not get blended properly (objects in front occlude/block it from view completely), so set a dummy object to be drawn last
		glPushMatrix();
		{
			glTranslatef(0.0f, 0.0f, 20.0f);
			glCallList(::lTriangle);
		}
		glPopMatrix();
		
		// swap sdl_gl double display buffer
		SDL_GL_SwapBuffers();
	}
}

namespace 
{
	void DrawCompass(GLfloat xr, GLfloat xg, GLfloat xb, GLfloat yr, GLfloat yg, GLfloat yb, GLfloat zr, GLfloat zg, GLfloat zb, GLfloat fAlpha)
	{
		// draw half cube
		glBegin(GL_QUADS); {
			glColor4f(xr, xg, xb, fAlpha);
			glVertex3f(0.05f, 0.05f, -0.05f); // left side, x is R
			glVertex3f(0.05f, 0.05f, 0.05f);
			glVertex3f(0.05f, -0.05f, 0.05f);
			glVertex3f(0.05f, -0.05f, -0.05f);
			
			glColor4f(yr, yg, yb, fAlpha);
			glVertex3f(-0.05f, 0.05f, 0.05f); // bottom side, y is G
			glVertex3f(0.05f, 0.05f, 0.05f);
			glVertex3f(0.05f, 0.05f, -0.05f);
			glVertex3f(-0.05f, 0.05f, -0.05f);
			
			glColor4f(zr, zg, zb, fAlpha);
			glVertex3f(-0.05f, 0.05f, -0.05f);	// front side, z is B
			glVertex3f(0.05f, 0.05f, -0.05f);
			glVertex3f(0.05f, -0.05f, -0.05f);
			glVertex3f(-0.05f, -0.05f, -0.05f);
		} glEnd();
		
		
		// draw right arrow body
		glBegin(GL_QUAD_STRIP); {
			glColor4f(xr, xg, xb, fAlpha);
			glVertex3f(0.2f, 0.01f, 0.01f); // front side
			glVertex3f(0.0f, 0.01f, 0.01f);
			glVertex3f(0.2f, -0.01f, 0.01f);
			glVertex3f(0.0f, -0.01f, 0.01f);
			
			glVertex3f(0.2f, -0.01f, -0.01f); // bottom side
			glVertex3f(0.0f, -0.01f, -0.01f);
			
			glVertex3f(0.2f, 0.01f, -0.01f); // back side
			glVertex3f(0.0f, 0.01f, -0.01f);
			
			glVertex3f(0.2f, 0.01f, 0.01f); // top side
			glVertex3f(0.0f, 0.01f, 0.01f);
		} glEnd();
		// draw right arrow head
		glBegin(GL_QUADS); {
			glVertex3f(0.2f, 0.04f, 0.04f);
			glVertex3f(0.2f, 0.04f, -0.04f);
			glVertex3f(0.2f, -0.04f, -0.04f);
			glVertex3f(0.2f, -0.04f, 0.04f);
		} glEnd();
		glBegin(GL_TRIANGLE_FAN); {
			glVertex3f(0.3f, 0.0f, 0.0f);
			glVertex3f(0.2f, 0.1f, -0.1f);
			glVertex3f(0.2f, 0.1f, 0.1f);
			glVertex3f(0.2f, -0.1f, 0.1f);
			glVertex3f(0.2f, -0.1f, -0.1f);
			glVertex3f(0.2f, 0.1f, -0.1f);
		} glEnd();
		
		
		// draw up arrow body
		glBegin(GL_QUAD_STRIP); {
			glColor4f(yr, yg, yb, fAlpha);
			glVertex3f(0.01f, 0.2f, -0.01f); // back side
			glVertex3f(0.01f, 0.0f, -0.01f);
			glVertex3f(-0.01f, 0.2f, -0.01f);
			glVertex3f(-0.01f, 0.0f, -0.01f);
			
			glVertex3f(-0.01f, 0.2f, 0.01f); // left side
			glVertex3f(-0.01f, 0.0f, 0.01f);
			
			glVertex3f(0.01f, 0.2f, 0.01f); // front side
			glVertex3f(0.01f, 0.0f, 0.01f);
			
			glVertex3f(0.01f, 0.2f, -0.01f); // right side
			glVertex3f(0.01f, 0.0f, -0.01f);
		} glEnd();
		// draw up arrow head
		glBegin(GL_QUADS); {
			glVertex3f(0.04f, 0.2f, 0.04f);
			glVertex3f(-0.04f, 0.2f, 0.04f);
			glVertex3f(-0.04f, 0.2f, -0.04f);
			glVertex3f(0.04f, 0.2f, -0.04f);
		} glEnd();
		glBegin(GL_TRIANGLE_FAN); {
			glVertex3f(0.0f, 0.3f, 0.0f);
			glVertex3f(-0.1f, 0.2f, 0.1f);
			glVertex3f(0.1f, 0.2f, 0.1f);
			glVertex3f(0.1f, 0.2f, -0.1f);
			glVertex3f(-0.1f, 0.2f, -0.1f);
			glVertex3f(-0.1f, 0.2f, 0.1f);
		} glEnd();
		
		
		// draw forward arrow body (forward arrow is opposite of z direction to follow rhr)
		glBegin(GL_QUAD_STRIP); {
			glColor4f(zr, zg, zb, fAlpha);
			glVertex3f(-0.01f, 0.01f, -0.2f); // left side
			glVertex3f(-0.01f, 0.01f, 0.0f);
			glVertex3f(0.01f, 0.01f, -0.2f);
			glVertex3f(0.01f, 0.01f, 0.0f);
			
			glVertex3f(0.01f, -0.01f, -0.2f); // top side
			glVertex3f(0.01f, -0.01f, 0.0f);
			
			glVertex3f(-0.01f, -0.01f, -0.2f); // right side
			glVertex3f(-0.01f, -0.01f, 0.0f);
			
			glVertex3f(-0.01f, 0.01f, -0.2f); // bottom side
			glVertex3f(-0.01f, 0.01f, 0.0f);
		} glEnd();
		// draw forward arrow head
		glBegin(GL_QUADS); {
			glVertex3f(-0.04f, 0.04f, -0.2f);
			glVertex3f(-0.04f, -0.04f, -0.2f);
			glVertex3f(0.04f, -0.04f, -0.2f);
			glVertex3f(0.04f, 0.04f, -0.2f);
		} glEnd();
		glBegin(GL_TRIANGLE_FAN); {
			glVertex3f(0.0f, 0.0f, -0.3f);
			glVertex3f(-0.1f, 0.1f, -0.2f);
			glVertex3f(0.1f, 0.1f, -0.2f);
			glVertex3f(0.1f, -0.1f, -0.2f);
			glVertex3f(-0.1f, -0.1f, -0.2f);
			glVertex3f(-0.1f, 0.1f, -0.2f);
		} glEnd();
	}
}

namespace 
{
	void DrawGround(float fExtent, float fStep, float y)
	{	
		// draw lines parallel to z-axis
		glBegin(GL_LINES); {
			for (float i=-fExtent; i<=fExtent; i+=fStep)
			{
				glVertex3f(i, y, -fExtent);
				glVertex3f(i, y, fExtent);
			}
		} glEnd();
		
		// draw lines parallel to x-axis
		glBegin(GL_LINES); {
			for (float i=-fExtent; i<=fExtent; i+=fStep)
			{
				glVertex3f(-fExtent, y, i);
				glVertex3f(fExtent, y, i);
			}
		} glEnd();
	}
}

namespace 
{
	void DrawSea(float x, float z, float fStep, float y)
	{	
		// draw lines parallel to z-axis
		glBegin(GL_LINES); {
			for (float i=-x; i<=x; i+=fStep)
			{
				glVertex3f(i, y, -z);
				glVertex3f(i, y, z);
			}
		} glEnd();
		
		// draw lines parallel to x-axis
		glBegin(GL_LINES); {
			for (float i=-z; i<=z; i+=fStep)
			{
				glVertex3f(-x, y, i);
				glVertex3f(x, y, i);
			}
		} glEnd();
	}
}

namespace 
{
	void DrawSphere(double r, int lats, int longs)
	{
		int i, j;
		for ( i = 0; i <= lats; ++i )
		{
			double lat0 = M_PI * (-0.5 + (double)(i - 1) / lats);
			double z0  = std::sin(lat0) * r;
			double zr0 =  std::cos(lat0) * r;
			
			double lat1 = M_PI * (-0.5 + (double)i / lats);
			double z1 = std::sin(lat1) * r;
			double zr1 = std::cos(lat1) * r;
			
			glBegin(GL_LINE_STRIP); {
				for ( j = 0; j <= longs; ++j )
				{
					double lng = 2 * M_PI * (double)(j - 1) / longs;
					double x = std::cos(lng);
					double y = std::sin(lng);
					
					glNormal3f(x * zr0, y * zr0, z0);
					glVertex3f(x * zr0, y * zr0, z0);
					glNormal3f(x * zr1, y * zr1, z1);
					glVertex3f(x * zr1, y * zr1, z1);
				}
			} glEnd();
		}
	}
}

namespace 
{
	void DrawTriangle(void)
	{
		glBegin(GL_TRIANGLES); {	// draw triangle facing +z
			glColor4f(1.0f, 0.0f, 0.0f, 0.9f);
			glVertex3f(0.0f, 0.5f, 0.0f);	// top
			glColor4f(0.0f, 1.0f, 0.0f, 0.4f);
			glVertex3f(-0.5f, -0.5f, 0.0f);	// bot left
			glColor4f(0.0f, 0.0f, 1.0f, 0.1f);
			glVertex3f(0.5, -0.5f, 0.0f);	// bot right
		} glEnd();
	}
}

namespace 
{
	void DrawDevil(float r, float g, float b, float a, GLMmodel* devil)
	{
		if ( devil )
		{
			glPushMatrix();
			{	
				glTranslatef(0.0f, -0.4f, 0.0f);
				glRotatef(0.0f, 0.0f, 1.0f, 0.0f);
				glScalef(1.6f, 1.3f, 1.6f);
				glColor4f(r, g, b, a);
				glmDraw(devil, GLM_NONE);
			}
			glPopMatrix();
		}
	}
}

namespace 
{
	void DrawPerson(float r, float g, float b, float a, GLMmodel* person)
	{
		if ( person )
		{
			glPushMatrix();
			{	
				glTranslatef(0.0f, -0.52f, 0.0f);
				glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
				glScalef(0.5f, 0.6f, 0.6f);
				//glColor4f(r, g, b, a);
				glmDraw(person, GLM_COLOR);
			}
			glPopMatrix();
		}
	}
}

namespace 
{
	void DrawBoat(float r, float g, float b, float a, GLMmodel* boat)
	{
		if ( boat )
		{
			glPushMatrix();
			{	
				glTranslatef(0.0f, 0.31f, 0.0f);
				glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
				glScalef(0.5f, 1.0f, 0.8f);
				//glColor4f(r, g, b, a);
				glmDraw(boat, GLM_COLOR);
			}
			glPopMatrix();
		}
	}
}

namespace 
{
	void DrawHeart(float r, float g, float b, float a, GLMmodel* heart)
	{
		if ( heart )
		{
			glPushMatrix();
			{	
				glTranslatef(0.0f, -0.45f, 0.0f);
				glScalef(0.18f, 0.19f, 0.35f);
				glColor4f(r, g, b, a);
				glmDraw(heart, GLM_NONE);
			}
			glPopMatrix();
		}
	}
}

namespace 
{
	void DrawDuck(float r, float g, float b, float a, GLMmodel* duck)
	{
		if ( duck )
		{
			glPushMatrix();
			{	
				glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
				glScalef(0.5f, 0.5f, 0.5f);
				glColor4f(r, g, b, a);
				glmDraw(duck, GLM_NONE);
			}
			glPopMatrix();
		}
	}
}

namespace 
{
	void DrawBox(float s, float alpha)
	{
		s /= 2.0f; // half the side length
		
		glBegin(GL_LINE_STRIP); {
			glColor4f(0.0f, 0.0f, 0.0f, alpha); // bottom
			glVertex3f(-s, -s, -s);
			glVertex3f(-s, -s, s);
			glVertex3f(s, -s, s);
			glVertex3f(s, -s, -s);
			glVertex3f(-s, -s, -s);
			
			glVertex3f(-s, s, -s); // front
			glVertex3f(s, s, -s);
			glVertex3f(s, -s, -s);
			glVertex3f(s, s, -s);
			
			glVertex3f(s, s, s); // top
			glVertex3f(-s, s, s);
			glVertex3f(-s, s, -s);
			glVertex3f(-s, s, s);
			
			glVertex3f(-s, -s, s); // back
			glVertex3f(s, -s, s);
			glVertex3f(s, s, s);
		} glEnd();
	}
}

namespace 
{
	void DrawXZCollisionSquare(float s, float h, float alpha)
	{
		s /= 2.0f;	// half the side length (length from center)
		h /= 2.0f;
		
		glBegin(GL_LINE_STRIP); {
			glColor4f(0.0f, 0.0f, 0.0f, alpha); // bottom
			glVertex3f(-s, -h, -s);
			glVertex3f(-s, -h, s);
			glVertex3f(s, -h, s);
			glVertex3f(s, -h, -s);
			glVertex3f(-s, -h, -s);
		} glEnd();
		
		glBegin(GL_LINE_STRIP); {
			glColor4f(0.0f, 0.0f, 0.0f, alpha); // top
			glVertex3f(-s, h, -s);
			glVertex3f(-s, h, s);
			glVertex3f(s, h, s);
			glVertex3f(s, h, -s);
			glVertex3f(-s, h, -s);
		} glEnd();
	}
}

namespace 
{
	void DrawCircle(float radius, float y, float alpha)
	{
		glBegin(GL_TRIANGLE_FAN); {
			glColor4f(0.0f, 0.0f, 0.0f, alpha);
			
			glVertex3f(0.0f, y, 0.0f);
			for ( float angle = 0.0f; angle < 360.0f; ++angle)
			{
				glVertex3f(-std::cos(angle)*radius, y, std::sin(angle)*radius);
			}
		} glEnd();
	}
}

namespace 
{
	// horizontal resizing is fine... but vertical resizing moves objects in opposite vertical direction
	void Reshape(GLsizei w, GLsizei h)
	{
		// handle division by zero
		if ( !h )
			h = 1;
		
		// set aspect ratio
		GLdouble dAspect;
		dAspect = (GLdouble)w / (GLdouble)h;
		
		// set viewport to window dimensions
		glViewport(0, 0, w, h);
		
		// reset coordinate system for what is projected onto the screen
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		// set perspective view
		GLdouble fovY = 30.0;
		gluPerspective(fovY*2.0, dAspect, 0.1, 1024.0);	// fovY = atan(y/x) = theta = top of eye's sight to ground
		
		// load modelview for transforming objects
		glMatrixMode(GL_MODELVIEW);
	}
}

namespace 
{
	void Quit(void)
	{
		// quit SDL_mixer
		Mix_CloseAudio();
		Mix_Quit();
		
		// quit SDL
		SDL_Quit();
	}
}





























